## Администрирование PostgreSQL 13 Базовый курс (DBA1)

мой конспект теории и заметками о прохождении практических заданий курса

## Данные о курсе

Авторы курса: Егор Рогов, Павел Лузанов, Илья Баштанов
Postgres Professional, 2015–2022

keywords: Postgres, Postgres pro, Postgres Professional (Software Company),
PostgreSQL (Software), Free Software (Software Genre), Software (Industry),
Постгрес, Постгрес Про

https://postgrespro.ru/education/courses/DBA1
https://edu.postgrespro.ru/dba1-13/dba1_student_guide.pdf
https://www.youtube.com/playlist?list=PLaFqU3KCWw6LPcuYVymLcXl3muC45mu3e


Предварительные знания:
- минимальные сведения о базах данных и SQL;
- знакомство с Unix.

Какие навыки будут получены:
- общие сведения об архитектуре PostgreSQL;
- установка, базовая настройка, управление сервером;
- организация данных на логическом и физическом уровнях;
- базовые задачи администрирования;
- управление пользователями и доступом;
- представление о резервном копировании и репликации.

Перед началом самостоятельного прохождения курса ознакомьтесь с руководством
слушателя: https://edu.postgrespro.ru/dba1-13/dba1_student_guide.pdf
В нем вы найдете инструкцию по настройке рабочего места и указания по выполнению практических заданий.


Виртуальная машина курса
https://edu.postgrespro.ru/DBA1-student-13.ova

```http
HTTP/1.1 200 OK
Last-Modified: Fri, 15 Jul 2022 05:26:41 GMT
ETag: "9db00200-5e3d1435b3cf3"
Accept-Ranges: bytes
Content-Length: 2645557760
```

Последняя версия учебных материалов курса находится по адресу:
https://edu.postgrespro.ru/DBA1-handouts-13.zip


## Конфиги

[конфиг psqlrc](./configs/.psqlrc)

```sh
sudo cp configs/.psqlrc /var/lib/postgresql/.psqlrc
```

набор своих кастомных команд для удобства работы внутри psql

```sh
sudo -su postgres
# [sudo] password for user:

postgres@debian:/home/user$ cd
postgres@debian:~$ pwd
# /var/lib/postgresql

# открытие сеанса работы с PostgreSQL через psql от лица пользователя postgres
postgres@debian:/home/user$ psql
Timing is on.
psql (13.14 (Debian 13.14-0+deb11u1))
Type "help" for help.

postgres@[local] ~=#
```

