## 02-DBA1-13. 02. Использование psql

- запуск psql и подключение к ДБ
- Получение справочной информации
- работа с psql
- настройка

## Кратко
- psql - терминальный(консольный) клиент для работы с СУБД PostgreSQL
- psql - это просто клиент который подключается к серверу, а значит ему нужны
  параметры того как и куда подключаться. Это можно указывать через параметры
  при вызове самой команды, либо через настроечные(конфиг) файлы.
- есть два типа команд которые поддерживает psql - это SQL и psql-команды(`\?`)


В уроке 01 было про pg_clt(pg_ctlcluster) - утилита для управления самим
PostgreSQL - сервером (программой в ОС) (Запуск и остановка)


## PSQL - клиентское приложение для подключения к CУБД PostgreSQL

Дальше для работы нужна возможность подключения к работающему серверу
для выполнения запросов и получения ответов на них.
Эту роль и выполняет клиентское приложение `psql`
- psql - терминальный клиент (консольная утилита без GUI)
- поставляется вместе с пакетом разработчиками самого PostgreSQL
- клиенты с GUI для подключения к БД самими разработчиками PostgreSQL не
  поставляются.
- любой админ должен уметь в командную строку а значит и в psql.
- psql удобен для автоматизации задач, обработки скриптов и интерактивного
  взаимодействия с сервером

Note: Здесь слово сервер будет обозначать сервер программы PostgreSQL (СУБД)

## Подключение к базе.

```sh
psgl -d <database> -U <user_role> -h <host> -p <port>
```
- database - имя конкретной базы данных в кластере базданных к которой нужно
  подключиться.
  помним что один инстанс-сервера PostgreSQL(СУБД) может управлять несколькими
  Базами Данных хранимыми на диске - совокупность которых и называется
  "Кластер Баз Данных"
- user_role - имя пользователя от лица которого подключаемся к серверу и бд.
- host - узел(хост) на который хотим подключится(можно и по сети ходить в СУБД)
- port - порт на котором PostgreSQL слушает входящие подключения


Новое подключение внутри `psgl` не выходя из него.
(Например подключиться от лица другого пользователя или к другой бд)
```psql
=> \c[onnect] database userrole host port
```

Узнать о том к чему сейчас подключены ("Где я?")
```psql
=>\conninfo
```


## Получение справки

Невозможно освоить всё за раз и всё помнить, но важно уметь находить нужное.
Места где можно побольше узнать про psql:
> командная строка ОС
- pgsl --help
- man psql

> Внутри самого psql(внутри оболочки)
- \?                 список всех поддерживаемых команд `psql`
- \? variables       переменные psgl
- \h[elp]            синтаксис команды SQL
- \q                 выход из оболочки psql


psql - это клиент позволяющий отправлять команды на сервер и получать ответы
от сервера, чтобы показать его пользователю.
- сервер работает через SQL-команды. т.е. для общения с сервером PostgreSQL
нужно выполнять SQL-команды. Но для удобства внутри сомого psql есть и свои
встроенные команды (все они начинаются с обратного слэша).


## Два Типа команд поддерживаемые psgl

- команды SQL которые отправляем на сервер PostgreSQL
- команды самого клиента(оболочки) psql (это все команды начинаются с `\`)



## Обкатка psql в консоли.

В виртуальной машине курса просто набрав psql сразу пройдёт подключение к БД.
```sh
# запуск по короткому имени команды(программы - будет искать полный из $PATH)
user@debian:~$ psgl

# или через полный путь если не находит по короткому(не прописан в $PATH)
user@debian:~$ /usr/lib/postgresql/13/bin/psql

# или сначала войдя в каталог где лежит программа и затем запуская через ./
user@debian:~$ cd /usr/lib/postgresql/13/bin/
user@debian:~$ ./psql
```

Note: зачем `./`
> префикс `./` перед именем исполняемого файла говорит:
> "возми файл из текущего каталога".
> Почему в Unix не запускает исполняемые файлы без этого префикса?
> В целях безопасности. По короткому имени файл либо берётся из $PATH либо
  выдаётся ошибка - файл не найден даже если он лежит в текущем каталоге.


Если же PostgreSQL ставим сами, руками либо в свою основную ОС либо в виртуалку
а не используем подготовленный авторами курса образ виртуалки, тогда при попытке
просто запустить без доп параметров выдаст такое:
```sh
user@debian:~$ psql
psql: error: FATAL:  role "user" does not exist
```
Что это за ошибка и все тонкости о правах доступа и как это работает и где об
этом почитать смотри файл ./notes/02-c-1-group-access.md.
Здесь же готовый рецепт того, как сделать чтобы просто работало:

Входим в режим работы с ОС от лица пользователя `postgres`.
(Чтобы это сработало нужно чтобы текущий пользователь(здесь это user) был
прописан в группе sudo. [Как это делается см](./notes/02-c-1-group-access.md))
```sh
user@debian:~$ sudo -su postgres
[sudo] password for user:            -- Здесь будет ввод пароля без отображения
postgres@debian:/home/user$

postgres@debian:~$ psql
psql (13.14 (Debian 13.14-0+deb11u1))
Type "help" for help.

postgres=#
```

Проверяем текущее подключение. Куда конкретно нас подключило?
```sh
postgres=# \conninfo
You are connected to database "postgres" as user "postgres"
via socket in "/var/run/postgresql" at port "5432".
```
- connected to database "postgres" - это имя БД к которой подключились
- user "postgres" - имя пользователя от лица которого подключились
- via socket in "/var/run/postgresql" at port "5432" - говорит о том как
  было осуществлено подключение к серверу PostgreSQL. в частности здесь
  подключение прошло через socket ОС с именем `/var/run/postgresql` через
  порт 5432

В курсе при выполнении этой команды при обычном подключении через `psql` без
аргументов подключение идёт сразу в бд student от именим юзера student.


## Выполнение команд SQL и форматирование результатов

Основное назначение утилиты psql - это отправка запроса на сервер PostgreSQL
чтобы получить от него ответ(результат) и показать этот ответ пользователю.
Показывать может по-разному: либо на экран либо записав например в файл.
Так же psql умеет форматировать результы запросов (ответы от сервера PostgreSQL)
в разные форматы.

Несколько наиболее популярных видов форматирования:
- формат с выравниванием значений
- формат без выравнивания
- расширенный формат

### как идёт отправка запроса на сервер
- пишем SQL запрос, можно в несколько строк.
- символ `;` в конце SQL-команды приведёт к отправке этой команды на сервер
  (т.е. её выполнению и получению ответа от сервера и его отображению на экране)

Этот запрос пишется внутри psql:
```sql
SELECT schemaname, tablename, tableowner
FROM pg_tables
LIMIT 5;
```
ответ
```sql
 schemaname |       tablename       | tableowner
------------+-----------------------+------------
 pg_catalog | pg_statistic          | postgres
 pg_catalog | pg_type               | postgres
 pg_catalog | pg_foreign_table      | postgres
 pg_catalog | pg_authid             | postgres
 pg_catalog | pg_statistic_ext_data | postgres
(5 rows)

postgres=#
```
что делает эта SQL-команда:
- покажи 3 столбца с именами schemaname, tablename, tableowner
- из конкретной таблицы с именем pg_tables
- ограничив выборку 5 строками(rows)

pg_tables - некая системная таблица, хранящая список таблиц которые есть в бд
к которой мы сейчас подключены.

такой вывод - это форматирование с выравниванием (это работает по умолчанию)
здесь все значения выравниваются под самое длинное значение(строку) в столбце
конкретно здесь это `pg_statistic_ext_data`

- `schemaname | tablename | tableowner` - это строка с названием столбцов(шапка)
- `(5 rows)` - сколько строк из БД было выбрано в результате SQL-Запроса(подвал)
- `|` - символ разделяющий столбцы

## меняем формат вывода результата

Команды psql для переключения режима выравнивания:
- `\a` — переключает режим с выравниванием и без выравнивания;
- `\t` — переключает отображения строки заголовка и итоговой строки.


```sh
postgres=# \a                          # переключаем режим в "без выравнивания"
Output format is unaligned.

postgres=# SELECT schemaname, tablename, tableowner FROM pg_tables LIMIT 5;

schemaname|tablename|tableowner
pg_catalog|pg_statistic|postgres
pg_catalog|pg_type|postgres
pg_catalog|pg_foreign_table|postgres
pg_catalog|pg_authid|postgres
pg_catalog|pg_statistic_ext_data|postgres
(5 rows)


postgres=# \t                          # переключение режима "только записи"
Tuples only is on.

postgres=# SELECT schemaname, tablename, tableowner FROM pg_tables LIMIT 5;

pg_catalog|pg_statistic|postgres
pg_catalog|pg_type|postgres
pg_catalog|pg_foreign_table|postgres
pg_catalog|pg_authid|postgres
pg_catalog|pg_statistic_ext_data|postgres


postgres=# \pset fieldsep ' '
Field separator is " ".

postgres=# SELECT schemaname, tablename, tableowner FROM pg_tables LIMIT 5;

pg_catalog pg_statistic postgres
pg_catalog pg_type postgres
pg_catalog pg_foreign_table postgres
pg_catalog pg_authid postgres
pg_catalog pg_statistic_ext_data postgres
```

таким образом мы изменили формат вывода результата ответа сервера

меняем обратно в как было
```sh
postgres=# \t \a
Tuples only is off.
Output format is aligned.
```


## \x расширенный формат, когда большое количество столбцов.

удобен когда столбцов слишком много и все они не помещаются в экран
разворачивает столбцы с горизонтали в вертикаль.


```sh
postgres=# \x
Expanded display is on.
```
```sql
SELECT * FROM pg_tables WHERE tablename = 'pg_class';
```
в этой команде запрашиваем все столбцы(SELECT `*`) из таблицы pg_tables
показывать только те строки поле tablename (название колонки), которой содержит
в себе строковое значение 'pg_class'

название_столбца | значение
```sh
-[ RECORD 1 ]-----------
schemaname  | pg_catalog
tablename   | pg_class
tableowner  | postgres
tablespace  |
hasindexes  | t
hasrules    | f
hastriggers | f
rowsecurity | f

# выключаю обратно - и смотрим как это же будет выглядеть в обычном режиме
postgres=# \x
Expanded display is off.
postgres=# SELECT * FROM pg_tables WHERE tablename = 'pg_class';
 schemaname | tablename | tableowner | tablespace | hasindexes | hasrules | hastriggers | rowsecurity
------------+-----------+------------+------------+------------+----------+-------------+-------------
 pg_catalog | pg_class  | postgres   |            | t          | f        | f           | f
(1 row)
```



### выполнение отдельного запроса в расширенном(\x) формате

Для этого вместо `;` в конце SQL Запроса пишем `\gx`
- `\g` - нечто вроде `go` то есть поехали - отправляй запрос
- `\gx` - поехали в расширенном формате(к ответу сервера примени потом `\x`).

```sh
postgres=# SELECT * FROM pg_tables WHERE tablename = 'pg_proc' \gx
-[ RECORD 1 ]-----------
schemaname  | pg_catalog
tablename   | pg_proc
tableowner  | postgres
tablespace  |
hasindexes  | t
hasrules    | f
hastriggers | f
rowsecurity | f
```


### \pset посмотреть какие еще есть форматы форматирования

- вообще через эту команду задаётся конкретное значение для конкретного
параметра форматирования. Но если не указать аргумент то выведет все имеющиеся
настройки.

```sh
postgres=#  \pset
border                   1
columns                  0
csv_fieldsep             ','
expanded                 off
fieldsep                 ' '
fieldsep_zero            off
footer                   on
format                   aligned
linestyle                ascii
null                     ''
numericlocale            off
pager                    1
pager_min_lines          0
recordsep                '\n'
recordsep_zero           off
tableattr
title
tuples_only              off
unicode_border_linestyle single
unicode_column_linestyle single
unicode_header_linestyle single
```



## Взаимодействие с ОС прямо изнутри psql

- `\! <cmd>` - запуск системных команд

```sh
postgres=# \! pwd
/var/lib/postgresql
```

- `\setenv` - установка переменной окружения для ОС (EnvVar)
```sh
postgres=# \setenv MY_VARNAME Hello
postgres=# \! echo $MY_VARNAME
Hello
```

- `\o <filename>` писать результаты запросов прямо в файл, а не выводить на экран
- `\o` - отключить режим вывода ответов сервера в заданный файл, (вернутся обратно)

```sh
postgres=# \o dba1_log
postgres=# SELECT schemaname, tablename, tableowner FROM pg_tables LIMIT 5;
# ответ - пусто т.к. всё записано прямо в файл - проверяем через команду cat:

postgres=# \! cat dba1_log
 schemaname |       tablename       | tableowner
------------+-----------------------+------------
 pg_catalog | pg_statistic          | postgres
 pg_catalog | pg_type               | postgres
 pg_catalog | pg_foreign_table      | postgres
 pg_catalog | pg_authid             | postgres
 pg_catalog | pg_statistic_ext_data | postgres
(5 rows)
```



## Использование psql для скриптов

- скрипт - это текстовый файл содержащий несколько команд которые можно
  выполнить все вместе за один раз.

```sh
SELECT format('SELECT count(*) FROM %I;', tablename)
FROM pg_tables LIMIT 3
\g (tuples_only=on format=unaligned) dba1_log
```

- `\g` - замена `;` он означает "SQL-запрос окончен, оотправляй на сервер".
- здесь в скобках после `\g` описаны настройки форматирования конкретно
  только для ответа на этот единичный запрос:
     - `tuples_only` - режим когда шапка и подвал при выводе скрываются т.е. \t
     - `format=unaligned` - без выравнивания по длине слов то есть `\a`
- `dba1_log` - это имя файла в который указываем сохранить результат
- LIMIT 3 - ограничиваем число строк которые хотим получить в ответе сервера.
- `SELECT count(*) FROM %I;` - это то что будет выведено в файл как результат
  `%I` - плейсхолдер который в строке будет заменён на значение из заданного
  столбца `tablename`

Таким образом здесь через SQL-запрос и оператор SELECT формируется список команд
для скрипта.

Выполняем эту команду и смотрим что она запишет в файл dba1_log
```sh
postgres=# \! cat dba1_log
SELECT count(*) FROM pg_statistic;
SELECT count(*) FROM pg_type;
SELECT count(*) FROM pg_foreign_table;
```

В итоге мы получили несколько SQL-команд пригодных для выполнения в psql
т.е. получили скрипт для psql из трёх SQL-команд


### `\g | cmd` прямая передача результата через канал(pipe) в нужную команду

можно результат запроса не писать в файл а сразу через пайп (|) передать на вход
нужной команды(cmd). Например можно передать результат в `grep` для поиска строк



## `\i[nclude]` - пакетный запуск скриптовых файлов

```sh
# до этого переключи вывод результатов запросов в конкретный файл, возвращаем
# вывод ответов сервера в обыный режим - вывод на экран.
\o

# создаём скрипт записывая его в файл dba1_log
postgres=# SELECT format('SELECT count(*) FROM %I;', tablename)
FROM pg_tables LIMIT 3
\g (tuples_only=on format=unaligned) dba1_log

# смотрим что записалось в этот файл
postgres=# \! cat dba1_log
SELECT count(*) FROM pg_statistic;
SELECT count(*) FROM pg_type;
SELECT count(*) FROM pg_foreign_table;

# Запускаем файл как скрипт в режиме пакетного запуска (все команды разом)
postgres=# \i dba1_log
 count
-------
   402
(1 row)

 count
-------
   411
(1 row)

 count
-------
     0
(1 row)
```

## еще способы как можно запускать psql-скрипты на выполнение

```sh
psql < filename
psql -f filename
```


## \gexec помошник для автоматизации задач

- выполнить каждую строку ответа на SQL-запрос как SQL-команду и отобразить
результат
- бывает полезно админам при администрированию сервера,
  для подготовки и выполнении задач по администрированию сервера.

В предыщем примере через оператор SELECT сначала формировались SQL команды
затем они записывались в файл образуя тем самым скрипт который можно выполнять,
и только потом шел запуск команд из этого скриптового-файла

`\gexec` - позволяет сделать тоже самое. но без файла как промежуточного звена.
т.е. создать SQL-Запрос, результатом которого станет несколько строк являющихся
SQL-запросами которые тоже можно выполнить. И далее все эти строки выполнить
как SQL-запросы. Т.е. по сути тоже самое как с файлом только без файла.

```sh
SELECT format('SELECT count(*) FROM %I;', tablename)
FROM pg_tables LIMIT 3
\gexec

 count
-------
   402
(1 row)

 count
-------
   411
(1 row)

 count
-------
     0
(1 row)
```

Здесь все те же 3 запроса и 3 ответа на них вывело на экран.



## Переменные psql и Управляющие конструкции

- внутренние переменные psql которые можно использовать в скриптах.
- имена переменных регистро-зависимые: ON и on - это две разных переменных
- их можно использовать в условном операторе if(который так же поддерживается
  на уровне psql)
- еще раз здесь речь идёт НЕ об переменных окружения ОС EnvVar, а о внутренних
  встроенных в сам psql перменных.


Создание переменной - `\set <varname> <value>`
(значения всегда строкового типа).
```sh
postgres=# \set MYVAR Yo!
postgres=# \echo :MYVAR
Yo!
postgres=# \unset MYVAR
postgres=# \echo :MYVAR
:MVAR
```

- создали переменную MYVAR со значением Yo!
- вывели её содержимое `\echo :MYVAR` (`:` перед именем переменной)
- удалили переменную `\unset`
- когда переменной нет `\echo` просто выводит её имя а не её значение

Назначение psql-переменных:
- сохранить ответ из БД в переменную для дальнейшей работы с ним как значением.
- чтобы при последующем запросе можно было бы подставить значение сохраненное
  в переменной.

```sh
postgres=# SELECT now() AS curr_time \gset
postgres=# \echo curr_time
curr_time
postgres=# \echo :curr_time
2024-03-23 12:16:40.871529+03
```
здесь \gset означает:
- \g - отправит запрос на сервер и получив ответ - применить для каждой строки:
- \set для каждой строки ответа. (здесь это будет одна строка с временем)
- now() - текущее дата и время

Если в ответе будет несколько строк то это работать НЕ будет. т.к. \set
умеет только с одними строками.

- получить список всех установленных переменных `\set`:

```sh
postgres=# \set
AUTOCOMMIT = 'on'
COMP_KEYWORD_CASE = 'preserve-upper'
DBNAME = 'postgres'
ECHO = 'none'
ECHO_HIDDEN = 'off'
ENCODING = 'UTF8'
ERROR = 'true'
FETCH_COUNT = '0'
HIDE_TABLEAM = 'off'
HISTCONTROL = 'none'
HISTSIZE = '500'
HOST = '/var/run/postgresql'
IGNOREEOF = '0'
LAST_ERROR_MESSAGE = 'syntax error at or near "("'
LAST_ERROR_SQLSTATE = '42601'
ON_ERROR_ROLLBACK = 'off'
ON_ERROR_STOP = 'off'
PORT = '5432'
PROMPT1 = '%/%R%x%# '
PROMPT2 = '%/%R%x%# '
PROMPT3 = '>> '
QUIET = 'off'
ROW_COUNT = '0'
SERVER_VERSION_NAME = '13.14 (Debian 13.14-0+deb11u1)'
SERVER_VERSION_NUM = '130014'
SHOW_CONTEXT = 'errors'
SINGLELINE = 'off'
SINGLESTEP = 'off'
SQLSTATE = '42601'
USER = 'postgres'
VERBOSITY = 'default'
VERSION = 'PostgreSQL 13.14 (Debian 13.14-0+deb11u1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 10.2.1-6) 10.2.1 20210110, 64-bit'
VERSION_NAME = '13.14 (Debian 13.14-0+deb11u1)'
VERSION_NUM = '130014'
curr_time = '2024-03-23 12:16:40.871529+03'
```

как видно в самом конце есть переменная созданная нами только что - `curr_time`


## Условные операторы \if \else \endif

хороши в скриптах

Пример.
- есть переменная содержащая каталог с рабочими файлами working_dir
- убедится что переменная определена можно через `\echo :{?<имя-переменной>}`

```sh
postgres=# \echo :{?working_dir}
FALSE
```
это можно использовать в условном операторе
```sql
\if :{?working_dir}
-- переменная определена
\else
-- в качестве значения можно установить результат выполнения команды ОС
\set working_dir `pwd`
\endif
```
таким образом такой скрипт когда переменная не задана задаст ей значение из
вывода системной команды `pwd` - т.е. установит в неё значение текущего каталога
`\if` умеет работать только с `TRUE` и `FALSE`


проверяем еще раз - теперь переменная задана
```sh
postgres=# \echo :{?working_dir}
TRUE
postgres=# \echo :working_dir
/var/lib/postgresql
```


## \d Получить описание таблицы, знакомство со структурой таблицы pg_tables

```sh
postgres=# \d pg_tables
              View "pg_catalog.pg_tables"
   Column    |  Type   | Collation | Nullable | Default
-------------+---------+-----------+----------+---------
 schemaname  | name    |           |          |
 tablename   | name    |           |          |
 tableowner  | name    |           |          |
 tablespace  | name    |           |          |
 hasindexes  | boolean |           |          |
 hasrules    | boolean |           |          |
 hastriggers | boolean |           |          |
 rowsecurity | boolean |           |          |
```
здесь видим уточнение что на деле pg_tables это не таблица а View(представление)
Здесь показано какие колонки есть у этой таблицы и какие типы данных для них
заданы.

Команд начинающихся с `\d` очень много. Посмотреть на их все можно через `\?`
внутри psql:
```sh
postgres=# \?
Informational
  (options: S = show system objects, + = additional detail)
  \d[S+]                 list tables, views, and sequences
  \d[S+]  NAME           describe table, view, sequence, or index
  \da[S]  [PATTERN]      list aggregates
  \dA[+]  [PATTERN]      list access methods
  \dAc[+] [AMPTRN [TYPEPTRN]]  list operator classes
  \dAf[+] [AMPTRN [TYPEPTRN]]  list operator families
  \dAo[+] [AMPTRN [OPFPTRN]]   list operators of operator families
  \dAp[+] [AMPTRN [OPFPTRN]]   list support functions of operator families
  \db[+]  [PATTERN]      list tablespaces
  \dc[S+] [PATTERN]      list conversions
  \dC[+]  [PATTERN]      list casts
  \dd[S]  [PATTERN]      show object descriptions not displayed elsewhere
  \dD[S+] [PATTERN]      list domains
  \ddp    [PATTERN]      list default privileges
  \dE[S+] [PATTERN]      list foreign tables
  \det[+] [PATTERN]      list foreign tables
  \des[+] [PATTERN]      list foreign servers
  \deu[+] [PATTERN]      list user mappings
  \dew[+] [PATTERN]      list foreign-data wrappers
  \df[anptw][S+] [PATRN] list [only agg/normal/procedures/trigger/window] functions
  \dF[+]  [PATTERN]      list text search configurations
  \dFd[+] [PATTERN]      list text search dictionaries
  \dFp[+] [PATTERN]      list text search parsers
  \dFt[+] [PATTERN]      list text search templates
  \dg[S+] [PATTERN]      list roles
  \di[S+] [PATTERN]      list indexes
  \dl                    list large objects, same as \lo_list
  \dL[S+] [PATTERN]      list procedural languages
  \dm[S+] [PATTERN]      list materialized views
  \dn[S+] [PATTERN]      list schemas
  \do[S+] [PATTERN]      list operators
  \dO[S+] [PATTERN]      list collations
  \dp     [PATTERN]      list table, view, and sequence access privileges
  \dP[itn+] [PATTERN]    list [only index/table] partitioned relations [n=nested]
  \drds [PATRN1 [PATRN2]] list per-database role settings
  \dRp[+] [PATTERN]      list replication publications
  \dRs[+] [PATTERN]      list replication subscriptions
  \ds[S+] [PATTERN]      list sequences
  \dt[S+] [PATTERN]      list tables
  \dT[S+] [PATTERN]      list data types
  \du[S+] [PATTERN]      list roles
  \dv[S+] [PATTERN]      list views
  \dx[+]  [PATTERN]      list extensions
  \dy[+]  [PATTERN]      list event triggers
```
Все эти команды предназначены для просмотра разного рода информации о объектах
системного каталога(?) об этом будет позже


## Настройка spql

- в Unix многие утилиты имеют конфигурационные скрипты: обще-системный скрипт и
  скрипты для настройки под каждого конкретного пользователя.
- такие конфигурационные скрипты есть и для psql. это файлы, которые
  выполняются при запуске psql.
  конфигурационный файл для конкретного пользователя берётся из ~/.psqlrc
- по умолчанию оба файла отсутствуют и пользовательский ~/.psqlrc и
  обще-системный.
  Узнать где находится обще-системный каталог конфигурации можно так:
  `pg_config --sysconfdir` в `/etc/postgresql-common/`

~/.psqlrc возможности настройки:
- можно настроить приглашение-подсказку которое показывает psgl
  по умолчанию выводится только имя базы данных. Можно туда добавить еще
  и имя пользователя от которого зашел в бд, порт и даже хост СУБД и т.д.
- настройка постраничного просмоторщика(less) для удобного просмотра вывода,
  который не вмещается в один экран.
- задавать переменные содержащие часто выполняемые длинные SQL-запросы
  это будут своего рода короткие команды для запуска больших SQL-запросов


### top5 - список самых больших таблиц в текущей БД

Например, запишем в переменную top5 - "список самых больших таблиц в текущей БД"
текст запроса на получение пяти самых больших по размеру таблиц:
```sql
\set top5 'SELECT tablename, pg_total_relation_size(schemaname||''.''||tablename)
AS bytes FROM pg_tables ORDER BY bytes DESC LIMIT 5;'
```
чтобы теперь выполнить этот запрос - просто вводим имя переменной `:top`
```sh
postgres=# :top5
   tablename    |  bytes
----------------+---------
 pg_depend      | 1130496
 pg_proc        | 1048576
 pg_rewrite     |  704512
 pg_attribute   |  671744
 pg_description |  581632
(5 rows)
```

Для того чтобы эта команда(:top5) стала доступна при каждом запуске `psql`
её и следует записать в конфиг-файл `~/.psqlrc`


в Unix библиотека `readline` делает работу в psql более удобной и комфортной:
- ведёт историю введённых ранее команд
- позволяет листать команды по истории чтобы не вводить из заново(через стрелки)
- авто-дополнение команд после ввода первых 3х символов (через ТАБ)
- двойное нажание таб - покажет список доступных команд при авто-дополнении
- pqsl имеет авто-дополнение не только для команд но и для названия объектов
  (т.е. имён таблиц и колонок в БД)

Поэтому важно при сборке PostgreSQL из исходников настроить использование
библиотеки readline.

## Итог
- psql - терминальный(консольный) клиент для работы с СУБД PostgreSQL
- psql - это просто клиент, который подключается к серверу, а значит ему нужны
  параметры того, как и куда подключаться. Это можно указывать через параметры
  при вызове самой команды, либо через конфигурационные(настроечные) файлы.
- есть два типа команд которые поддерживает psql - это SQL и psql-команды(`\?`)


## Практика

1. Запустите psql и проверьте информацию о текущем подключении.
2. Выведите все строки таблицы pg_tables.
3. Установите команду `less -XS` для постраничного просмотра и
   еще раз выведите все строки pg_tables.
4. Приглашение по умолчанию показывает только имя базы данных.
   Настройте приглашение так, чтобы дополнительно выводилась информация о
   пользователе: `роль@база=#`
5. Настройте psql так, чтобы для всех команд выводилась длительность выполнения.
   Убедитесь, что при повторном запуске эта настройка сохраняется.

## Заметки:
роль из 4 пункта - это понятие в PostgreSQL которым обозначаются имена
пользователей при их доступе к БД.
В материалах курса есть комментарии для прохождения практики(подсказки)
- есть ответы от авторов курса(читерный способ прохождения практики).

## Практика+

1. Откройте транзакцию и выполните команду, которая завершается любой ошибкой.
Убедитесь, что продолжить работу в этой транзакции невозможно.
2. Установите переменной `ON_ERROR_ROLLBACK` значение `on` и убедитесь, что
после ошибки можно продолжать выполнять команды внутри транзакции.

[моё прохождение практики](./practice/02-p-psql.md)
[моё прохождение практики+](./practice/02-p2-psql.md)


