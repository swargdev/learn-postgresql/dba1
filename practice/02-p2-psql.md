## Практика+ Утилита psql и обработка ошибок

### Зачем?
Транзакция - это блок из несколько команд которые применяются сразу пачкой.
При этом либо все команды проходят успешно и накатываются на БД. либо если
есть хотябы одна ошибка при исполнении команды ни одна команда из транзакции
не проходит и БД не измненяет своё состояние. Проще говоря транзакция это
применить все команды разом, а если есть ошибки - ни одну их них.
Транзакции нужны т.к. бывает нужно атомарно вносить измнения в бд. но при этом
сами измнения требуют того, чтобы они производились несколькими командами.
и если первые прошли успешно и поменяли фактическое состояние бд, а последняя
прошла с ошибкой то надо будет как-то откатывать успешные команды. именно эту
задачу и решают транзакции.

При вводе команд в режиме транзакции по умолчанию значение ON_ERROR_ROLLBACK
равно off. И в таком режиме внутри транзакции при вводе команды с ошибкой
"вся транзакция" падает ломая ввод всех предыдущих команд. т.е. чтобы
продолжить транзакцию(т.е. ввод блока команд как единого целого) надо не просто
исправить текущую команду с ошибкой но и заново вводить все ранее введённые
команды т.к. они не накатились, а команда с ошибкой поломала всю транзакцию.
Т.е. все ранее введённые команды сбросились и надо вводить всё заново.


Задания:

1. Откройте транзакцию и выполните команду, которая завершается любой ошибкой.
   Убедитесь, что продолжить работу в этой транзакции невозможно.

2. Установите переменной `ON_ERROR_ROLLBACK` значение `on` и убедитесь, что
   после ошибки можно продолжать выполнять команды внутри транзакции.

### Коменнтарии
1. Для открытия транзакции выполните команду BEGIN;

2. Установка переменной `ON_ERROR_ROLLBACK` заставляет psql устанавливать точку
сохранения (SAVEPOINT) перед каждой SQL командой в открытой транзакции и
в случае ошибки откатываться к этой точке сохранения.
https://postgrespro.ru/docs/postgresql/13/sql-savepoint



Открыть транзацию  - `BEGIN;` закрыть - `COMMIT;`

```sh
postgres@[local] ~=# create database ctd2;
CREATE DATABASE

postgres@[local] ~=# \c ctd2;
You are now connected to database "ctd2" as user "postgres".

postgres@[local] ctd2=# CREATE TABLE groups(group_id int, group_no char(6));
CREATE TABLE


postgres@[local] ctd2=# select * from groups;
 group_id | group_no
----------+----------
(0 rows)


# открываю транзакцию
ostgres@[local] ctd2=# BEGIN;
BEGIN

# просто вставляю новые строки в таблицу (без ошибок)
postgres@[local] ctd2=*# INSERT INTO groups(group_id, group_no) VALUES
postgres@[local] ctd2-*# (1, 'GR1'), (2, 'GR2');
INSERT 0 2

# ошибка которая должна поломать цепоку команд в текущей транзакции
postgres@[local] ctd2=*# INSERT INTO groups(group_id, group_no) VALUES
(1, 'GR1'), (2, 'GR2', 3);
ERROR:  VALUES lists must all be the same length
LINE 2: (1, 'GR1'), (2, 'GR2', 3);
                     ^

# Завершение транзакции чтобы посмотреть прошла ли первая команда вставки 2х строк
postgres@[local] ctd2=!# COMMIT;
ROLLBACK

# как видно не прошла - данные не записались т.к. вторая команда была с ошибкой
postgres@[local] ctd2=# select * from groups;
 group_id | group_no
----------+----------
(0 rows)
```

Теперь тоже самое с ON_ERROR_ROLLBACK=on

```sh
postgres@[local] ctd2=# \set ON_ERROR_ROLLBACK on
postgres@[local] ctd2=# \echo :ON_ERROR_ROLLBACK
on

# Открываю транзакцию
postgres@[local] ctd2=# BEGIN;
BEGIN

# вставляю в таблицу две новых записи
postgres@[local] ctd2=*# INSERT INTO groups(group_id, group_no) VALUES
(1, 'GR1'), (2, 'GR2');
INSERT 0 2

# команда с ошибкой
postgres@[local] ctd2=*# INSERT INTO groups(group_id, group_no) VALUES
(1, 'GR1'), (2, 'GR2', 3);
ERROR:  VALUES lists must all be the same length
LINE 2: (1, 'GR1'), (2, 'GR2', 3);
                     ^

# завершаю транзакцию "коммитом" т.е. фиксированием всех сделанных команд
postgres@[local] ctd2=*# COMMIT;
COMMIT

# проверяю прошла ли первая команда до команды с ошибкой
postgres@[local] ctd2=# select * from groups;
 group_id | group_no
----------+----------
        1 | GR1
        2 | GR2
(2 rows)
```

Видно что 1я команда прошла т.к. данные в таблице появились.


## Ответы от авторов
```sh
student$ psql
```

Утилита `psql` по умолчанию работает в режиме автоматической фиксации транзакций.
Поэтому любая команда SQL выполняется в отдельной транзакции.

Чтобы явно начать транзакцию, нужно выполнить команду BEGIN:
```sh
student@student=# BEGIN;
BEGIN
```
Обратите внимание на то, что приглашение psql изменилось.
Символ «звездочка» говорит о том, что транзакция сейчас активна.

```sh
student@student=*# CREATE TABLE t (id int);
CREATE TABLE

# Предположим, мы случайно сделали ошибку в следующей команде:
student@student=*# INSERTINTO t VALUES(1);
ERROR: syntax error at or near "INSERTINTO"
LINE 1: INSERTINTO t VALUES(1);
^
```
О случившейся ошибке можно узнать из приглашения:
звездочка изменилась на восклицательный знак. Попробуем исправить команду:
```sh
student@student=!# INSERT INTO t VALUES(1);
ERROR: current transaction is aborted, commands ignored until end of transaction block
```
Но PostgreSQL не умеет откатывать только одну команду транзакции, поэтому
транзакция обрывается и откатывается целиком.
Чтобы продолжить работу, мы должны выполнить команду завершения транзакции.
Не важно, будет ли это COMMIT или ROLLBACK, ведь транзакция уже отменена.

```sh
student@student=!# COMMIT;
ROLLBACK

#Создание таблицы было отменено, поэтому ее нет в базе данных:
student@student=# SELECT * FROM t;
ERROR: relation "t" does not exist
LINE 1: SELECT * FROM t;
^
```

2. Переменная ON_ERROR_ROLLBACK
Изменим поведение psql.
```sh
student@student=# \set ON_ERROR_ROLLBACK on
```
Теперь перед каждой командой транзакции неявно будет устанавливаться
точка сохранения(SAVEPOINT), а в случае ошибки будет происходить автоматический
откат к этой точке. Это позволит продолжить выполнение команд транзакции.
```sh

student@student=# BEGIN;
BEGIN

student@student=*# CREATE TABLE t (id int);
CREATE TABLE

student@student=*# INSERTINTO t VALUES(1);
ERROR: syntax error at or near "INSERTINTO"
LINE 1: INSERTINTO t VALUES(1);
^

student@student=*# INSERT INTO t VALUES(1);
INSERT 0 1

student@student=# SELECT * FROM t;
 id
----
  1
(1 row)
```

Переменной ON_ERROR_ROLLBACK можно установить значение interactive, тогда
подобное поведение будет только в интерактивном режиме работы,
но не при выполнении скриптов.

<!-- 26-03-2024 15:09:10 > -->
<!-- 26-03-2024 15:35:47 , решено! -->
<!-- 26-03-2024 15:48:19 < повторил решения авторов -->

