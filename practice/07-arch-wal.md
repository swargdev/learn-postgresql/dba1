### Практика

> Задания:

1. Средствами операционной системы найдите процессы, отвечающие за работу
   буферного кеша и журнала WAL.

2. Остановите PostgreSQL в режиме `fast`; снова запустите его.
   Просмотрите журнал сообщений сервера.

3. Теперь остановите в режиме `immediate` и снова запустите.
   Просмотрите журнал сообщений сервера и сравните с предыдущим разом.


> Краткое описание что делать и зачем:

- Посмотреть на работающие в PostgreSQL процессы, найти среди процессов те которые
отвечают за работу буферного кэша и WAL-журнала
- попробовать останавливать PostgreSQL в различных режимах(fast и immediate)
  - fast - перед выключением СУБД выполняет контрольную точку(это означает что
  все "грязные" данные с буферного кэша сбрасываются на диск, и после останова
  сервер запускается немедленно)
  - immediate - сброса "грязных" страниц с оперативки на диск не происходит -
  то есть всё в буферном кэше теряется, а при запуске сервера происходит
  восстановление по WAL-журналу, такое же как после сбоя.

В общем цель приостанавливать СУБД в различных этих режимах и посмотреть журнал
сообщений и найти информацию что именно происходит при запуске СУБД после
останова при каждом из этих режимов.


> Коментарии(Подсказки к выполнению заданий)

2. Для останова в режиме fast используйте команду
```sh
pg_ctlcluster 13 main stop
```
При этом сервер обрывает все открытые соединения и перед выключением выполняет
контрольную точку, чтобы на диск записались согласованные данные.
Таким образом, выключение может выполняться относительно долго, но при запуске
сервер сразу же будет готов к работе.

3. Для останова в режиме immediate используйте команду
```sh
pg_ctlcluster 13 main stop -m immediate --skip-systemctl-redirect
```
При этом сервер также обрывает открытые соединения, но не выполняет контрольную
точку. На диске остаются несогласованные данные, как после сбоя.
Таким образом, выключение происходит быстро, но при запуске сервер должен будет
восстановить согласованность данных с помощью журнала.

Для PostgreSQL, собранного из исходных кодов, останов в режиме fast выполняется
командой
```sh
pg_ctl stop
```
а останов в режиме immediate командой
```sh
pg_ctl stop -m immediate
```



## Моё Решение

### 1. Процессы операционной системы

Сначала получим идентификатор процесса `postmaster`.
Он записан в первой строке файла postmaster.pid.
Этот файл расположен в каталоге с данными и создается каждый раз при СУБД
```sh
sudo cat /var/lib/postgresql/13/main/postmaster.pid
# 141596
# /var/lib/postgresql/13/main
# 1713719965
# 5432
# /var/run/postgresql
# localhost
#    265304     98315
# ready
```

Смотрим все процессы, порожденные процессом `postmaster`:
```sh
sudo ps -o pid,command --ppid 141596
    PID COMMAND
 # 141598 postgres: 13/main: checkpointer                            <<
 # 141599 postgres: 13/main: background writer                       <<
 # 141600 postgres: 13/main: walwriter                               <<
 # 141601 postgres: 13/main: autovacuum launcher
 # 141602 postgres: 13/main: stats collector
 # 141603 postgres: 13/main: logical replication launcher
 # 141657 postgres: 13/main: postgres postgres [local] idle
```

К процессам, обслуживающим буферный кеш и WAL-журнал, можно отнести:
  - checkpointer;
  - background writer;
  - walwriter.



### 2. Остановка в режиме fast
```sh
sudo pg_ctlcluster 13 main stop
```

Смотрю системный журнал, как проходил останов PostgreSQL сервера

Все подробности в логе лежащем в /var/log/postgresql/postgresql-13-main.log
```sh
less /var/log/postgresql/postgresql-13-main.log
# 2024-04-22 10:58:22.318 MSK [141596] LOG:  received fast shutdown request
# 2024-04-22 10:58:22.356 MSK [141596] LOG:  aborting any active transactions
# 2024-04-22 10:58:22.356 MSK [141657] postgres@postgres FATAL:  terminating connection due to administrator command
# 2024-04-22 10:58:22.357 MSK [141596] LOG:  background worker "logical replication launcher" (PID 141603) exited with exit code 1
# 2024-04-22 10:58:22.358 MSK [141598] LOG:  shutting down
# 2024-04-22 10:58:22.533 MSK [141596] LOG:  database system is shut down
```

Поднимаю сервер после fast-останова
```sh
sudo pg_ctlcluster 13 main start

less /var/log/postgresql/postgresql-13-main.log
# 2024-04-22 11:05:49.816 MSK [153855] LOG:  starting PostgreSQL 13.14 (Debian 13.14-0+deb11u1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 10.2.1-6) 10.2.1 20210110, 64-bit
# 2024-04-22 11:05:49.817 MSK [153855] LOG:  listening on IPv6 address "::1", port 5432
# 2024-04-22 11:05:49.817 MSK [153855] LOG:  listening on IPv4 address "127.0.0.1", port 5432
# 2024-04-22 11:05:49.858 MSK [153855] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
# 2024-04-22 11:05:49.955 MSK [153856] LOG:  database system was shut down at 2024-04-22 10:58:22 MSK
# 2024-04-22 11:05:49.994 MSK [153855] LOG:  database system is ready to accept connections
```

А что в общесистемном логе?

Обще-системный журнал будет содержать только факт останова, но без подробностей
```sh
sudo journalctl -xe
# Apr 22 10:58:22 debian systemd[1]: Stopping PostgreSQL Cluster 13-main...
# ░░ Subject: A stop job for unit postgresql@13-main.service has begun execution
# ░░ Defined-By: systemd
# ░░
# ░░ A stop job for unit postgresql@13-main.service has begun execution.
# ░░
# ░░ The job identifier is 65644.
# Apr 22 10:58:22 debian systemd[1]: postgresql@13-main.service: Succeeded.
# ░░ Subject: Unit succeeded
# ░░ Defined-By: systemd
# ░░
# ░░ The unit postgresql@13-main.service has successfully entered the 'dead' state.
# Apr 22 10:58:22 debian systemd[1]: Stopped PostgreSQL Cluster 13-main.
# ░░ Subject: A stop job for unit postgresql@13-main.service has finished
# ░░ Defined-By: systemd
# ░░
# ░░ A stop job for unit postgresql@13-main.service has finished.
# ░░
# ░░ The job identifier is 65644 and the job result is done.
# Apr 22 10:58:22 debian systemd[1]: postgresql@13-main.service: Consumed 3.914s CPU time.
# ░░ Subject: Resources consumed by unit runtime
# ░░ Defined-By: systemd
# ░░ The unit postgresql@13-main.service completed and consumed the indicated resources.

# то как проходил запуск после fast-останова

# Apr 22 11:05:49 debian systemd[1]: Starting PostgreSQL Cluster 13-main...
# ░░ Subject: A start job for unit postgresql@13-main.service has begun execution
# ░░ Defined-By: systemd
# ░░
# ░░ A start job for unit postgresql@13-main.service has begun execution.
# ░░
# ░░ The job identifier is 65794.
# Apr 22 11:05:52 debian systemd[1]: Started PostgreSQL Cluster 13-main.
# ░░ Subject: A start job for unit postgresql@13-main.service has finished successfully
# ░░ Defined-By: systemd
# ░░
# ░░ A start job for unit postgresql@13-main.service has finished successfully.
# ░░
# ░░ The job identifier is 65794.
```

А вот данные о состоянии сервера из общесистемного `systemctl`:

посте fast-останова смотрю на состояние сервера - inactive (dead)
```sh
sudo systemctl status postgresql@13-main
# postgresql@13-main.service - PostgreSQL Cluster 13-main
#      Loaded: loaded (/lib/systemd/system/postgresql@.service; enabled-runtime; vendor preset: enabled)
#      Active: inactive (dead) since Mon 2024-04-22 10:58:22 MSK; 6min ago
#     Process: 141591 ExecStart=/usr/bin/pg_ctlcluster --skip-systemctl-redirect 13-main start (code=exited, status=0/SUCCESS)
#     Process: 153728 ExecStop=/usr/bin/pg_ctlcluster --skip-systemctl-redirect -m fast 13-main stop (code=exited, status=0/SUC>
#    Main PID: 141596 (code=exited, status=0/SUCCESS)
#         CPU: 3.914s
#
# Apr 21 20:19:25 debian systemd[1]: Starting PostgreSQL Cluster 13-main...
```

статус сервера через общесистемную команду systemctl после старта
```sh
sudo systemctl status postgresql@13-main
# ● postgresql@13-main.service - PostgreSQL Cluster 13-main
#      Loaded: loaded (/lib/systemd/system/postgresql@.service; enabled-runtime; vendor preset: enabled)
#      Active: active (running) since Mon 2024-04-22 11:05:52 MSK; 5s ago
#     Process: 153850 ExecStart=/usr/bin/pg_ctlcluster --skip-systemctl-redirect 13-main start (code=exited, status=0/SUCCESS)
#    Main PID: 153855 (postgres)
#       Tasks: 7 (limit: 16600)
#      Memory: 17.0M
#         CPU: 134ms
#      CGroup: /system.slice/system-postgresql.slice/postgresql@13-main.service
#              ├─153855 /usr/lib/postgresql/13/bin/postgres -D /var/lib/postgresql/13/main -c config_file=/etc/postgresql/13/ma>
#              ├─153857 postgres: 13/main: checkpointer
#              ├─153858 postgres: 13/main: background writer
#              ├─153859 postgres: 13/main: walwriter
#              ├─153860 postgres: 13/main: autovacuum launcher
#              ├─153861 postgres: 13/main: stats collector
#              └─153862 postgres: 13/main: logical replication launcher
#
# Apr 22 11:05:49 debian systemd[1]: Starting PostgreSQL Cluster 13-main...
# Apr 22 11:05:52 debian systemd[1]: Started PostgreSQL Cluster 13-main.
```


### 3. Останов сервера в режиме `immediate` и последующий запуск.
Просмотрите журнал сообщений сервера и сравните с предыдущим разом.

Резкий(immediate) останов:(без записи буферного кэша на диск)
```sh
sudo pg_ctlcluster 13 main stop -m immediate --skip-systemctl-redirect

# смотрю логи как шел останов сервера
less /var/log/postgresql/postgresql-13-main.log
# 2024-04-22 11:13:38.769 MSK [154051] LOG:  received immediate shutdown request
# 2024-04-22 11:13:38.806 MSK [154056] WARNING:  terminating connection because of crash of another server process
# 2024-04-22 11:13:38.806 MSK [154056] DETAIL:  The postmaster has commanded this server process to roll back the current transaction and exit, because another server process exited abnormally and possibly corrupted shared memory.
# 2024-04-22 11:13:38.806 MSK [154056] HINT:  In a moment you should be able to reconnect to the database and repeat your command.
# 2024-04-22 11:13:38.811 MSK [154051] LOG:  database system is shut down
```

```sh
# запуск сервера
sudo pg_ctlcluster 13 main start

# смотрю как прошел запуск сервера
less /var/log/postgresql/postgresql-13-main.log
# 2024-04-22 11:15:27.217 MSK [154124] LOG:  starting PostgreSQL 13.14 (Debian 13.14-0+deb11u1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 10.2.1-6) 10.2.1 20210110, 64-bit
# 2024-04-22 11:15:27.218 MSK [154124] LOG:  listening on IPv6 address "::1", port 5432
# 2024-04-22 11:15:27.218 MSK [154124] LOG:  listening on IPv4 address "127.0.0.1", port 5432
# 2024-04-22 11:15:27.252 MSK [154124] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
# 2024-04-22 11:15:27.332 MSK [154125] LOG:  database system was interrupted; last known up at 2024-04-22 11:12:57 MSK
# 2024-04-22 11:15:27.441 MSK [154125] LOG:  database system was not properly shut down; automatic recovery in progress
# 2024-04-22 11:15:27.479 MSK [154125] LOG:  redo starts at 0/BFB7020
# 2024-04-22 11:15:27.479 MSK [154125] LOG:  invalid record length at 0/BFB7058: wanted 24, got 0
# 2024-04-22 11:15:27.479 MSK [154125] LOG:  redo done at 0/BFB7020
# 2024-04-22 11:15:27.721 MSK [154124] LOG:  database system is ready to accept connections
```

Вот интересные строки о том, что бд была остановлена именно в режиме `immediate`
"database system was not properly shut down; automatic recovery in progress"


Интереса ради смотрю общесистемный журнал, а что там
```sh
# смотрим системный журнал как шел immediate-останов
sudo journalctl -xe

# Apr 22 11:13:38 debian systemd[1]: postgresql@13-main.service: Control process exited, code=exited, status=2/INVALIDARGUMENT
# ░░ Subject: Unit process exited
# ░░ Defined-By: systemd
# ░░
# ░░ An ExecStop= process belonging to unit postgresql@13-main.service has exited.
# ░░
# ░░ The process' exit code is 'exited' and its exit status is 2.
# Apr 22 11:13:38 debian systemd[1]: postgresql@13-main.service: Failed with result 'exit-code'.
# ░░ Subject: Unit failed
# ░░ Defined-By: systemd
# ░░
# ░░ The unit postgresql@13-main.service has entered the 'failed' state with result 'exit-code'.


# а вот как шел запуск после immediate-Останова

# Apr 22 11:15:27 debian sudo[154114]:  ... COMMAND=/usr/bin/pg_ctlcluster 13 >
# Apr 22 11:15:27 debian systemd[1]: Starting PostgreSQL Cluster 13-main...
# ░░ Subject: A start job for unit postgresql@13-main.service has begun execution
# ░░ Defined-By: systemd
# ░░
# ░░ A start job for unit postgresql@13-main.service has begun execution.
# ░░
# ░░ The job identifier is 66392.
# Apr 22 11:15:29 debian systemd[1]: Started PostgreSQL Cluster 13-main.
# ░░ Subject: A start job for unit postgresql@13-main.service has finished successfully
# ░░ Defined-By: systemd
# ░░
# ░░ A start job for unit postgresql@13-main.service has finished successfully.
# ░░
# ░░ The job identifier is 66392.
```


### Сравниваю результаты 2 & 3 заданий

#### останов fast и immediate

```sh
pg_ctlcluster 13 main stop                     # fast
# LOG:  received fast shutdown request
# LOG:  aborting any active transactions
# FATAL:  terminating connection due to administrator command
# LOG:  background worker "logical replication launcher" (PID 141603) exited with exit code 1
# LOG:  shutting down
# LOG:  database system is shut down
```


```sh
pg_ctlcluster 13 main stop -m immediate --skip-systemctl-redirect
# LOG:  received immediate shutdown request
# WARNING:  terminating connection because of crash of another server process
# DETAIL:  The postmaster has commanded this server process to roll back the current transaction and exit, because another server process exited abnormally and possibly corrupted shared memory.
# HINT:  In a moment you should be able to reconnect to the database and repeat your command.
# LOG:  database system is shut down
```

#### включение после fast и immediate

> после `fast`-выключения

- starting PostgreSQL 13.14 (Debian 13.14-0+deb11u1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 10.2.1-6) 10.2.1 20210110, 64-bit
- listening on IPv6 address "::1", port 5432
- listening on IPv4 address "127.0.0.1", port 5432
- listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
- database system was shut down at 2024-04-22 10:58:22 MSK
- database system is ready to accept connections


> после `immediate`-выключения

- starting PostgreSQL 13.14 (Debian 13.14-0+deb11u1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 10.2.1-6) 10.2.1 20210110, 64-bit
- listening on IPv6 address "::1", port 5432
- listening on IPv4 address "127.0.0.1", port 5432
- listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
- database system was interrupted; last known up at 2024-04-22 11:12:57 MSK
- database system was not properly shut down; automatic recovery in progress `<<`
- redo starts at 0/BFB7020
- invalid record length at 0/BFB7058: wanted 24, got 0
- redo done at 0/BFB7020
- database system is ready to accept connections


Вывод - после immediate-останова в логе чётко видно что при запуске идёт
восстановление состояния бд из WAL-журнала
`database system was not properly shut down; automatic recovery in progress`
а значит immediate - способ быстро вырубить но долго запускать тогда как
fast - долго вырубать но быстро запускаться


--------------------------------------------------------------------------------


### Решение авторов курса

1. Процессы операционной системы
Сначала получим идентификатор процесса postmaster.
Он записан в первой строке файла postmaster.pid. Этот файл расположен в каталоге
с данными и создается каждый раз при старте сервера.

```sh
student$ sudo cat /var/lib/postgresql/13/main/postmaster.pid
28650
/var/lib/postgresql/13/main
1705390505
5432
/var/run/postgresql
localhost
655372
32775
ready
```

Теперь смотрим все процессы, порожденные процессом postmaster:
student$ sudo ps -o pid,command --ppid 28650
PID       COMMAND
28652  postgres: 13/main: checkpointer
28653  postgres: 13/main: background writer
28654  postgres: 13/main: walwriter
28656  postgres: 13/main: stats collector
28657  postgres: 13/main: logical replication launcher
30977  postgres: 13/main: autovacuum launcher

К процессам, обслуживающим буферный кеш и журнал, можно отнести:

checkpointer; background writer; walwriter.


2. Остановка в режиме fast

Чтобы легко отделить старые сообщения от новых, мы просто удалим журнал
сообщений перед перезапуском сервера. Конечно, в реальной работе так поступать
не следует.
```sh
student$ sudo rm /var/log/postgresql/postgresql-13-main.log
student$ sudo pg_ctlcluster 13 main restart
```
Журнал сообщений сервера:
```sh
student$ cat /var/log/postgresql/postgresql-13-main.log
# 2024-01-16 10:35:31.566 MSK [31184] LOG: starting PostgreSQL 13.7 (Ubuntu 13.7-1.pgdg22.04+1) on x86_64-pc-linux-gnu, compiled by gcc (Ubuntu 11.2.0-19ubuntu1) 11.2.0, 64-bit
# 2024-01-16 10:35:31.566 MSK [31184] LOG: listening on IPv4 address "127.0.0.1", port 5432
# 2024-01-16 10:35:31.576 MSK [31184] LOG: listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
# 2024-01-16 10:35:31.598 MSK [31185] LOG: database system was shut down at 2024-01-16 10:35:31 MSK
# 2024-01-16 10:35:31.614 MSK [31184] LOG: database system is ready to accept connections
```

3. Остановка в режиме immediate
```sh
student$ sudo rm /var/log/postgresql/postgresql-13-main.log
student$ sudo pg_ctlcluster 13 main stop -m immediate --skip-systemctl-redirect
student$ sudo pg_ctlcluster 13 main start
```
Журнал сообщений сервера:

```sh
student$ cat /var/log/postgresql/postgresql-13-main.log
# 2024-01-16 10:35:34.261 MSK [31290] LOG: starting PostgreSQL 13.7 (Ubuntu 13.7-1.pgdg22.04+1) on x86_64-pc-linux-gnu, compiled by gcc (Ubuntu 11.2.0-19ubuntu1) 11.2.0, 64-bit
# 2024-01-16 10:35:34.261 MSK [31290] LOG: listening on IPv4 address "127.0.0.1", port 5432
# 2024-01-16 10:35:34.272 MSK [31290] LOG: listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
# 2024-01-16 10:35:34.298 MSK [31291] LOG: database system was interrupted; last known up at 2024-01-16 10:35:31 MSK
# 2024-01-16 10:35:41.584 MSK [31291] LOG: database system was not properly shut down; automatic recovery in progress
# 2024-01-16 10:35:41.596 MSK [31291] LOG: invalid record length at 0/DA0E680: wanted 24, got 0
# 2024-01-16 10:35:41.596 MSK [31291] LOG: redo is not required
# 2024-01-16 10:35:41.674 MSK [31290] LOG: database system is ready to accept connections
```

Перед тем, как начать принимать соединения, СУБД выполнила восстановление
(automatic recovery in progress).
