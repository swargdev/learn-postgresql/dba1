# DBA1 PostgreSQL-13 Из Курса Postgres Professional

Установка из пакена на Debian
/f/v/db/postgres-professional/13/dba1

```sh
sudo apt install postgresql postgresql-contrib

sudo systemctl status postgresql
sudo systemctl start postgresql
```

## Практика
Включение расчёта контрольных сумм в кластере.
1. Останови сервер
2. Проверь идёт ли расчёт контрольных сумм(контролек) в кластере
3. Включи расчёт контрольных сумм.
4. Запусти сервер
Доп материалы - важные коментарии есть есть внизу под слайдом в материалах курса.
там есть то как выполнить каку-нибудь неочевидную операцию

```sh
# 1 Stop the server
sudo pg_ctlcluster 13 main stop


sudo /usr/lib/postgresql/13/bin/pg_checksums --check -D /var/lib/postgresql/13/main/
pg_checksums: error: data checksums are not enabled in cluster

# 3 Enable checksum calculation
sudo /usr/lib/postgresql/13/bin/pg_checksums --enable -D /var/lib/postgresql/13/main/
Checksum operation completed
Files scanned:  916
Blocks scanned: 2973
pg_checksums: syncing data directory
pg_checksums: updating control file
Checksums enabled in cluster

# 4 Start the Server
sudo pg_ctlcluster 13 main start
# empty output - OK

# 5 Check Status
sudo systemctl status postgresql
● postgresql.service - PostgreSQL RDBMS
     Loaded: loaded (/lib/systemd/system/postgresql.service; enabled; vendor preset: enabled)
     Active: active (exited) since Wed 2024-03-20 12:16:31 MSK; 1h 12min ago
#            ^^^^^^

sudo pg_ctlcluster 13 main status
pg_ctl: server is running (PID: 155946)
/usr/lib/postgresql/13/bin/postgres "-D" "/var/lib/postgresql/13/main" "-c" "config_file=/etc/postgresql/13/main/postgresql.conf"
```


Возникшие вопросы

Как для sudo(рута) сделать чтобы утилиты запускались без полного пути
profile bashrc

```sh
# make postgresql tools executable:
export PATH="$PATH:/usr/lib/postgresql/13/bin/"

which pg_checksums
/usr/lib/postgresql/13/bin//pg_checksums
```

## Практика+
Установить PostgreSQL из исходников + инициализировать кластер баз данных

