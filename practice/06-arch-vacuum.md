## Практика
в этой теме есть только обычная, нет Практики+, но зато заданий много.

В экспериментальных целях отключить автоочистку, создать таблицу с большим
количеством данных, затем большими порциями обновлять эту таблицу смотря попутно
на разрастание размера таблицы. Убедится что размеры разбухают очень сильно.
Затем произвести полную очистку чтобы вернуться в первоначальное состояние.
Далее нужно будет повторить тоже самое но попутно еще и запуская между обновами
данных команду VACUUM для очистки. И посмотреть как при этом будет расти таблица

Главное что здесь надо будет понять и осознать - необходимость изменения данных
малыми порциями с запуском очистки между обновлениями.
Своими глазами увидеть что разница разных подходов может быть очень сильной.

1. Отключите процесс автоочистки и убедитесь, что он не работает.
2. В новой базе данных создайте таблицу с одним числовым столбцом и
   индекс по этой таблице. Вставьте в таблицу 100 000 случайных чисел.
3. Несколько раз измените половину строк таблицы,
   контролируя на каждом шаге размер таблицы и индекса.
4. Выполните полную очистку.
5. Повторите пункт 4, вызывая после каждого изменения обычную очистку.
   Сравните результаты.
6. Включите процесс автоочистки.


Комментарии

1. Установите параметр autovacuum в значение off и
попросите сервер перечитать файлы конфигурации.

3. Используйте функции pg_table_size(имя-таблицы) и pg_indexes_size(имя-таблицы).
Подробнее о функциях для вычисления размеров различных объектов говорится в
модуле «Организация данных».

6. Установите параметр autovacuum в значение on (или сбросьте значение этого
параметра командой RESET), затем попросите сервер перечитать файлы конфигурации.

--------------------------------------------------------------------------------


### Моё решение

1. Отключить автоочистку и убедиться что она точно не работает

```sql
ALTER SYSTEM SET autovacuum = off;
-- ALTER SYSTEM       запрос на отключение автоочистки на уровне всей СУБД

SELECT pg_reload_conf();
--  pg_reload_conf    запрос перечитать конфигурационный файл
-- ----------------
--  t
-- (1 row)


-- смотрю текущее значение параметра настройки autovacuum
SELECT name, setting, unit, boot_val, reset_val,
source, sourcefile, sourceline, pending_restart, context
FROM pg_settings
WHERE name = 'autovacuum' \gx

-- -[ RECORD 1 ]---+-------------------------------------------------
-- name            | autovacuum
-- setting         | off                 -- автоочистка выключена
-- unit            |
-- boot_val        | on
-- reset_val       | off
-- source          | configuration file
-- sourcefile      | /var/lib/postgresql/13/main/postgresql.auto.conf
-- sourceline      | 3
-- pending_restart | f
-- context         | sighup

SELECT pid, backend_start, backend_type
FROM pg_stat_activity
WHERE backend_type = 'autovacuum launcher';

--  pid | backend_start | backend_type     - автоочистка точно отключена
-- -----+---------------+--------------    - нет ни одного запущенного процесса
-- (0 rows)
```


2. В новой базе данных
   создаю таблицу с одним числовым столбцом и
   индекс по этой таблице.
   Вставляю в таблицу 100 000 случайных чисел.


```sql

CREATE DATABASE db1;
-- CREATE DATABASE
postgres@[local] ~=# \c db1;
-- You are now connected to database "db1" as user "postgres".

postgres@[local] db1=#

CREATE TABLE t(n integer);
-- CREATE TABLE
CREATE INDEX ON t(n);
-- CREATE INDEX

INSERT INTO t(n)
SELECT RANDOM() * 100000 FROM generate_series(1, 100000);
-- INSERT 0 100000     создано 100к строк из случайных чисел
-- Time: 732.272 ms

select * from t limit 4;
--    n                  для себя убеждаюсь что числа точно случайные.
-- -------
--  48604
--  83449
--  50495
--  75867
-- (4 rows)
```

3. Несколько раз измените половину строк таблицы,
   контролируя на каждом шаге размер таблицы и индекса.

 Используйте функции
- pg_table_size(имя-таблицы) и
- pg_indexes_size(имя-таблицы).

до измнения данных смотрю на первоночальный размер таблицы `t` в своей бд `bd1`
```sql
SELECT pg_table_size('t');
--  pg_table_size
-- ---------------
--        3653632          3 653 632 байт?
-- (1 row)

SELECT pg_indexes_size('t');
--  pg_indexes_size
-- -----------------
--          2441216        2 441 216 байт?
-- (1 row)
```

смотрю более подробно через расширение pgstattuple

```sql
SELECT * FROM pgstattuple('t') \gx
-- ERROR:  function pgstattuple(unknown) does not exist
-- LINE 1: SELECT * FROM pgstattuple('t')
--                       ^
-- HINT:  No function matches the given name and argument types. You might need to add explicit type casts.
```
то есть значит расширение нужно устанавливать для каждой базыданных отдельно

```sql
CREATE EXTENSION pgstattuple;
-- CREATE EXTENSION
SELECT * FROM pgstattuple('t') \gx
-- -[ RECORD 1 ]------+--------
-- table_len          | 3629056
-- tuple_count        | 100000
-- tuple_len          | 2800000
-- tuple_percent      | 77.16
-- dead_tuple_count   | 0
-- dead_tuple_len     | 0
-- dead_tuple_percent | 0
-- free_space         | 16652
-- free_percent       | 0.46

SELECT * FROM pgstatindex('t_n_idx') \gx
-- -[ RECORD 1 ]------+--------
-- version            | 4
-- tree_level         | 1
-- index_size         | 2441216
-- root_block_no      | 3
-- internal_pages     | 1
-- leaf_pages         | 296
-- empty_pages        | 0
-- deleted_pages      | 0
-- avg_leaf_density   | 78.23
-- leaf_fragmentation | 50
```

Для более удобного и быстрого просмотра размера создаю команду.
В переменную psql записываю запрос, который будет вычислять размер таблицы и
индекса.
```sql
\set SIZE 'SELECT pg_size_pretty(pg_table_size(''t'')) table_size, pg_size_pretty(pg_indexes_size(''t'')) index_size\\gx (footer=off)'

:SIZE                               -- запуск переменной psql как команды
-- table_size | 3568 kB
-- index_size | 2384 kB
--
```

первое измнение половины строк в таблице

для того чтобы указать условие для обхода половины строк использую
`WHERE n < 50000` вот так вот проверяю количество строк проходящих моё условие.
```sql
SELECT COUNT(*) FROM t WHERE n < 50000;
--  count
-- -------
--  50082
```

делаю первое измнение половины строк в таблице и смотрю на увеличение размера
```sql
-- 1th
UPDATE t SET n=n WHERE n < 50000;
-- UPDATE 50082
-- Time: 742.736 ms

:SIZE
-- table_size  | 5344 kB
-- index_size  | 3320 kB
--
```

еще нескольки обновлений(измнений) строк

```sql
-- 2th
UPDATE t SET n=n WHERE n < 50000;
-- UPDATE 50082
-- Time: 1050.157 ms (00:01.050)

:SIZE
--  table_size |  7112 kB
--  index_size |  3464 kB

-- 3th
UPDATE t SET n=n WHERE n < 50000;
-- UPDATE 50082
-- Time: 969.843 ms

:SIZE
--  table_size | 8880 kB
--  index_size | 3616 kB

UPDATE t SET n=n WHERE n < 50000;
-- UPDATE 50082
-- Time: 512.940 ms

:SIZE
--  table_size | 10 MB
--  index_size | 4584 kB
```

как итог - размер таблицы раздулся в три раза, индекса в дрва раза! было:
table_size = 3568 kB
index_size = 2384 kB

```sql
SELECT * FROM pgstattuple('t') \gx
-- -[ RECORD 1 ]------+---------      было
-- table_len          | 10887168      3.6M
-- tuple_count        | 100000
-- tuple_len          | 2800000
-- tuple_percent      | 25.72         77%
-- dead_tuple_count   | 50082
-- dead_tuple_len     | 1402296
-- dead_tuple_percent | 12.88
-- free_space         | 4844980       16K
-- free_percent       | 44.5
```

```sql
SELECT * FROM pgstatindex('t_n_idx') \gx
-- -[ RECORD 1 ]------+--------     was
-- version            | 4
-- tree_level         | 2
-- index_size         | 4694016      2.4M
-- root_block_no      | 412
-- internal_pages     | 4            1
-- leaf_pages         | 568          296
-- empty_pages        | 0
-- deleted_pages      | 0
-- avg_leaf_density   | 73.99        78
-- leaf_fragmentation | 49.47        50
```



4. Выполните полную очистку.
```sql
VACUUM FULL;
-- VACUUM
-- Time: 10746.169 ms (00:10.746)

:SIZE                        -- было изначально
-- table_size | 3544 kB              3568 kB
-- index_size | 1936 kB              2384 kB
```
Note:
- Размер таблицы даже стал чуть меньше чем было изначально,
- индекс стал компактнее
  (построить индекс по большому объему данных эффективнее,
  чем добавлять эти данные к индексу построчно).



5. Повторите пункт 4, вызывая после каждого изменения обычную очистку.

Замеряю размеры до измнений(полностью перестроенная таблица и индекс)
```sql
SELECT * FROM pgstattuple('t') \gx
-- -[ RECORD 1 ]------+--------
-- table_len          | 3629056
-- tuple_count        | 100000
-- tuple_len          | 2800000
-- tuple_percent      | 77.16
-- dead_tuple_count   | 0
-- dead_tuple_len     | 0
-- dead_tuple_percent | 0
-- free_space         | 16652
-- free_percent       | 0.46
```

```sql
SELECT * FROM pgstatindex('t_n_idx') \gx
-- -[ RECORD 1 ]------+--------
-- version            | 4
-- tree_level         | 1
-- index_size         | 1982464
-- root_block_no      | 3
-- internal_pages     | 1
-- leaf_pages         | 240
-- empty_pages        | 0
-- deleted_pages      | 0
-- avg_leaf_density   | 90.01
-- leaf_fragmentation | 0

:SIZE
-- table_size | 3544 kB
-- index_size | 1936 kB
```

делаю 4 последовательных обновлений вызывая каждый раз очистку и :SIZE
```sql
-- 1е обновление
UPDATE t SET n=n WHERE n < 50000;
-- UPDATE 50082
-- Time: 676.781 ms

VACUUM;
-- VACUUM
-- Time: 190.928 ms

:SIZE
-- table_size | 5352 kB
-- index_size | 2896 kB

-- 2e
UPDATE t SET n=n WHERE n < 50000;
-- UPDATE 50082
-- Time: 466.971 ms
VACUUM;
-- VACUUM
-- Time: 106.395 ms
:SIZE
-- table_size | 5352 kB
-- index_size | 2896 kB


-- 3e
UPDATE t SET n=n WHERE n < 50000;
-- UPDATE 50082
-- Time: 474.985 ms
VACUUM;
-- VACUUM
-- Time: 43.521 ms
:SIZE
-- table_size | 5352 kB
-- index_size | 2896 kB


-- 4e
UPDATE t SET n=n WHERE n < 50000;
-- UPDATE 50082
-- Time: 469.528 ms
VACUUM;
-- VACUUM
-- Time: 49.575 ms
:SIZE
-- table_size | 5352 kB
-- index_size | 2896 kB
```
Размер увеличился один раз и затем стабилизировался.
Пример показывает, что удаление (и изменение) большого объема данных
по возможности следует разделить на несколько транзакций.
Это позволит автоматической очистке своевременно удалять ненужные версии строк,
что позволит избежать чрезмерного разрастания таблицы.


```sql
SELECT * FROM pgstattuple('t');
-- table_len          | 5447680
-- tuple_count        | 100000
-- tuple_len          | 2800000
-- tuple_percent      | 51.4
-- dead_tuple_count   | 0
-- dead_tuple_len     | 0
-- dead_tuple_percent | 0
-- free_space         | 1628128
-- free_percent       | 29.89

SELECT * FROM pgstatindex('t_n_idx') \gx
-- version            | 4
-- tree_level         | 1
-- index_size         | 2965504
-- root_block_no      | 3
-- internal_pages     | 1
-- leaf_pages         | 360
-- empty_pages        | 0
-- deleted_pages      | 0
-- avg_leaf_density   | 60.85
-- leaf_fragmentation | 33.33
```

Сравниваю результаты полученные из этих дву проходов обновлений строк

```sql
-- UPDATE+:SIZE (измнение строк без очистки)
--                0        1th        2th       3th       4th
-- table_size | 3568 kB | 5344 kB |  7112 kB | 8880 kB | 10 MB
-- index_size | 2384 kB | 3320 kB |  3464 kB | 3616 kB | 4584 kB


-- UPDATE+VACUUM+:SIZE (измнение с очисткой)
--                 0       1th       2th        3th        4th
-- table_size | 3544 kB | 5352 kB | 5352 kB |  5352 kB  | 5352 kB
-- index_size | 1936 kB | 2896 kB | 2896 kB |  2896 kB  | 2896 kB
```

чётко видно что при полностью выключенной очистке 4 цикла обновления
половины таблицы раздувают размер этой таблицы почти в 3 раза, в то время
как точно такие же обновления но с простой очисткой `VACUUM` раздувают размер
почти в два раза меньше.


6. Включите процесс автоочистки.

```sql
ALTER SYSTEM SET autovacuum = on;
-- ALTER SYSTEM

SELECT pg_reload_conf();
 -- pg_reload_conf

SELECT pid, backend_start, backend_type
FROM pg_stat_activity
WHERE backend_type = 'autovacuum launcher';
--   pid   |        backend_start         |    backend_type
-- --------+------------------------------+---------------------
--  110093 | 2024-04-14 17:38:40.72398+03 | autovacuum launcher
-- (1 row)
```

