Здесь находится мои ответы по прохождению заданий Практика и Практика+.
И так же ответы авторов курса.

## Практика
## Задача 1.
Получи список параметров, для изменения которых требуется перезапуск сервера.

для получения нужно помнить что значение context равное postmaster и указывает
на то что данный параметр может вступить в силу только при рестарте сервера.

подробно
```sql
SELECT name, setting, unit,
source, sourcefile, sourceline
FROM pg_settings
WHERE context = 'postmaster' \g
```

только имена таких параметров, для которых требуется рестарт сервера.
```sql
SELECT name
FROM pg_settings
WHERE context = 'postmaster' \g
```

 archive_mode
 autovacuum_freeze_max_age
 autovacuum_max_workers
 autovacuum_multixact_freeze_max_age
 bonjour
 bonjour_name
 cluster_name
 config_file
 data_directory
 data_sync_retry
 dynamic_shared_memory_type
 event_source
 external_pid_file
 hba_file
 hot_standby
 huge_pages
 ident_file
 ignore_invalid_pages
 jit_provider
 listen_addresses
 logging_collector
 max_connections
 max_files_per_process
 max_locks_per_transaction
 max_logical_replication_workers
 max_pred_locks_per_transaction
 max_prepared_transactions
 max_replication_slots
 max_wal_senders
 max_worker_processes
 old_snapshot_threshold
 port
 recovery_target
 recovery_target_action
 recovery_target_inclusive
 recovery_target_lsn
 recovery_target_name
 recovery_target_time
 recovery_target_timeline
 recovery_target_xid
 restore_command
 shared_buffers
 shared_memory_type
 shared_preload_libraries
 superuser_reserved_connections
 track_activity_query_size
 track_commit_timestamp
 unix_socket_directories
 unix_socket_group
 unix_socket_permissions
 wal_buffers
 wal_level
 wal_log_hints
(53 rows)



```sql
select name, setting, unit, sourcefile
from pg_settings
where context = 'postmaster';
```
```

                name                 |                 setting                 | unit |               sourc>
-------------------------------------+-----------------------------------------+------+-------------------->
 archive_mode                        | off                                     |      |
 autovacuum_freeze_max_age           | 200000000                               |      |
 autovacuum_max_workers              | 3                                       |      |
 autovacuum_multixact_freeze_max_age | 400000000                               |      |
 bonjour                             | off                                     |      |
 bonjour_name                        |                                         |      |
 cluster_name                        | 13/main                                 |      | /etc/postgresql/13/>
 config_file                         | /etc/postgresql/13/main/postgresql.conf |      |
 data_directory                      | /var/lib/postgresql/13/main             |      |
 data_sync_retry                     | off                                     |      |
 dynamic_shared_memory_type          | posix                                   |      | /etc/postgresql/13/>
 event_source                        | PostgreSQL                              |      |
 external_pid_file                   | /var/run/postgresql/13-main.pid         |      | /etc/postgresql/13/>
 hba_file                            | /etc/postgresql/13/main/pg_hba.conf     |      |
 hot_standby                         | on                                      |      |
 huge_pages                          | try                                     |      |
 ident_file                          | /etc/postgresql/13/main/pg_ident.conf   |      |
 ignore_invalid_pages                | off                                     |      |
 jit_provider                        | llvmjit                                 |      |
 listen_addresses                    | localhost                               |      |
 logging_collector                   | off                                     |      |
 max_connections                     | 100                                     |      | /etc/postgresql/13/>
 max_files_per_process               | 1000                                    |      |
 max_locks_per_transaction           | 64                                      |      |
 max_logical_replication_workers     | 4                                       |      |
 max_pred_locks_per_transaction      | 64                                      |      |
 max_prepared_transactions           | 0                                       |      |
 max_replication_slots               | 10                                      |      |
 max_wal_senders                     | 10                                      |      |
 max_worker_processes                | 8                                       |      |
 old_snapshot_threshold              | -1                                      | min  |
 port                                | 5432                                    |      | /etc/postgresql/13/>
 recovery_target                     |                                         |      |
 recovery_target_action              | pause                                   |      |
 recovery_target_inclusive           | on                                      |      |
 recovery_target_lsn                 |                                         |      |
 recovery_target_name                |                                         |      |
 recovery_target_time                |                                         |      |
 recovery_target_timeline            | latest                                  |      |
 recovery_target_xid                 |                                         |      |
 restore_command                     |                                         |      |
 shared_buffers                      | 16384                                   | 8kB  | /etc/postgresql/13/>
 shared_memory_type                  | mmap                                    |      |
 shared_preload_libraries            |                                         |      |
 superuser_reserved_connections      | 3                                       |      |
 track_activity_query_size           | 1024                                    | B    |
 track_commit_timestamp              | off                                     |      |
 unix_socket_directories             | /var/run/postgresql                     |      | /etc/postgresql/13/>
 unix_socket_group                   |                                         |      |
 unix_socket_permissions             | 0777                                    |      |
 wal_buffers                         | 512                                     | 8kB  |
 wal_level                           | replica                                 |      |
 wal_log_hints                       | off                                     |      |
(53 rows)
```


## Задание 2: умение находить и исправлять ошибки в конфиге
В файле postgresql.conf сделай ошибку при изменении параметра max_connections.
Перезапусти сервер. Убедитесь, что сервер не стартует, и проверь журнал
сообщений. Исправь ошибку и запустите сервер.

### Коментарии авторов к задаче
- Расположение файла postgresql.conf можно найти,
  посмотрев значение параметра config_file.
- Редактируйте файл postgresql.conf либо от имени владельца –
  пользователя postgres), либо с правами суперпользователя.
  В первом случае удобно открыть новое окно терминала и выполнить в нем команду:
  ```sh
  sudo su postgres
  ```
  Во втором случае запускайте редактор из командной строки через команду sudo,
  например:
  ```sh
  sudo vim postgresql.conf
  ```


```sh
# смотрим расположение основного конфиг файла через SQL запрос внутри psgl:
# сначала вход в интерактивный режим клиентского приложения psql
psgl
```

SHOW config_file;  SHOW - команда показывающая значение конфиг-параметра.
```sh
postgres@debian:~$ psql
Timing is on.
psql (13.14 (Debian 13.14-0+deb11u1))
Type "help" for help.

postgres@[local] ~=# SHOW config_file;
               config_file
-----------------------------------------
 /etc/postgresql/13/main/postgresql.conf
(1 row)
```

Еще один способ прямо из консоли а не внутри psql узнать значение параметра
- здесь флаг `-c` означает выполнить команду вывести её ответ и завершиться
```sh
postgres@debian:~$ psql -c 'SHOW config_file;'
Timing is on.
               config_file
-----------------------------------------
 /etc/postgresql/13/main/postgresql.conf
(1 row)
```


- Добавляем значение с ошибкой
```sh
vi /etc/postgresql/13/main/postgresql.conf
# Add settings for extensions here
work_mem=8mB
#         ^^error valid is MB
```

перезапускаем сервер чтобы он подхватил измнения
```sh
postgres@debian:~$ pg_ctlcluster 13 main restart
Error: cluster is running from systemd, can only restart it as root. Try instead:
  sudo systemctl restart postgresql@13-main
# здесь просто нужно было указать sudo pg_ctlcluster, но можно и так:

user@debian:~$ sudo systemctl restart postgresql@13-main
Job for postgresql@13-main.service failed because the service did not take \
the steps required by its unit configuration.
See "systemctl status postgresql@13-main.service" and "journalctl -xe" for details.
```

Смотрим обще-системный журнал сообщений(логи), чтобы узнать что пошло не так
```sh
sudo journalctl -xe
```
Output:
```c
/*
Mar 27 08:03:23 debian systemd[1]: Starting PostgreSQL Cluster 13-main...
░░ Subject: A start job for unit postgresql@13-main.service has begun execution
░░ Defined-By: systemd
░░ Support: https://www.debian.org/support
░░
░░ A start job for unit postgresql@13-main.service has begun execution.
░░
░░ The job identifier is 278853.
Mar 27 08:03:24 debian postgresql@13-main[531495]: Error: /usr/lib/postgresql/13/bin/pg_ctl /usr/lib/postgresql/13/bin/pg_ctl start -D /var/lib/postgresql/13/main -l /var/log/postgresql/postgresql-13-main.log -s -o  -c config_file="/etc/postgresql/13/ma>
Mar 27 08:03:24 debian postgresql@13-main[531495]: 2024-03-27 08:03:23.955 MSK [531501] LOG:  invalid value for parameter "work_mem": "8mB"
Mar 27 08:03:24 debian postgresql@13-main[531495]: 2024-03-27 08:03:23.955 MSK [531501] HINT:  Valid units for this parameter are "B", "kB", "MB", "GB", and "TB".
Mar 27 08:03:24 debian postgresql@13-main[531495]: 2024-03-27 08:03:23.956 MSK [531501] FATAL:  configuration file "/etc/postgresql/13/main/postgresql.conf" contains errors
Mar 27 08:03:24 debian postgresql@13-main[531495]: pg_ctl: could not start server
Mar 27 08:03:24 debian postgresql@13-main[531495]: Examine the log output.
Mar 27 08:03:24 debian systemd[1]: postgresql@13-main.service: Can't open PID file /run/postgresql/13-main.pid (yet?) after start: Operation not permitted
Mar 27 08:03:24 debian systemd[1]: postgresql@13-main.service: Failed with result 'protocol'.
░░ Subject: Unit failed
░░ Defined-By: systemd
░░ Support: https://www.debian.org/support
░░
░░ The unit postgresql@13-main.service has entered the 'failed' state with result 'protocol'.
Mar 27 08:03:24 debian systemd[1]: Failed to start PostgreSQL Cluster 13-main.
░░ Subject: A start job for unit postgresql@13-main.service has failed
░░ Defined-By: systemd
░░ Support: https://www.debian.org/support
░░
░░ A start job for unit postgresql@13-main.service has finished with a failure.
░░
░░ The job identifier is 278853 and the job result is failed.
...
*/
```

как видно, здесь даже показывает что не так и где ошибка
```sh
LOG:  invalid value for parameter "work_mem": "8mB"
HINT:  Valid units for this parameter are "B", "kB", "MB", "GB", and "TB".
FATAL:  configuration file "/etc/postgresql/13/main/postgresql.conf" contains errors
```
искать начала ошибки можно по фразе `Starting PostgreSQL Cluster 13-main...`


Запускаю упавший сервер PostgreSQL
```sh
user@debian:~/dev/2024/db/postgresql/dba1$ pg_ctlcluster 13 main start
Warning: the cluster will not be running as a systemd service. Consider using systemctl:
  sudo systemctl start postgresql@13-main
Error: You must run this program as the cluster owner (postgres) or root

# ошибка выше говорит что не хватило прав у pg_ctlcluster переключиться на postgres
sudo pg_ctlcluster 13 main start
# тихо отработало ничего не вернув

# смотрим текущее состояние сервера
sudo pg_ctlcluster 13 main status
pg_ctl: server is running (PID: 531751)
/usr/lib/postgresql/13/bin/postgres "-D" "/var/lib/postgresql/13/main" \
  "-c" "config_file=/etc/postgresql/13/main/postgresql.conf"
```

Еще один общесистемный способ(для systemd) посмотреть состояние сервиса(демона)
```sh
sudo systemctl status postgresql
● postgresql.service - PostgreSQL RDBMS
     Loaded: loaded (/lib/systemd/system/postgresql.service; enabled; vendor preset: enabled)
     Active: active (exited) since Thu 2024-03-21 09:14:27 MSK; 5 days ago
    Process: 203143 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
   Main PID: 203143 (code=exited, status=0/SUCCESS)
        CPU: 1ms

Mar 21 09:14:27 debian systemd[1]: Starting PostgreSQL RDBMS...
Mar 21 09:14:27 debian systemd[1]: Finished PostgreSQL RDBMS.

sudo systemctl status postgresql@13-main
● postgresql@13-main.service - PostgreSQL Cluster 13-main
     Loaded: loaded (/lib/systemd/system/postgresql@.service; enabled-runtime; vendor preset: enabled)
     Active: active (running) since Wed 2024-03-27 08:14:03 MSK; 2min 14s ago
    Process: 531746 ExecStart=/usr/bin/pg_ctlcluster --skip-systemctl-redirect 13-main start (code=exited, status=0/SUCCESS)
   Main PID: 531751 (postgres)
      Tasks: 7 (limit: 16600)
     Memory: 20.1M
        CPU: 221ms
     CGroup: /system.slice/system-postgresql.slice/postgresql@13-main.service
             ├─531751 /usr/lib/postgresql/13/bin/postgres -D /var/lib/postgresql/13/main -c config_file=/etc/postgresql/13/main/postgresql.conf
             ├─531753 postgres: 13/main: checkpointer
             ├─531754 postgres: 13/main: background writer
             ├─531755 postgres: 13/main: walwriter
             ├─531756 postgres: 13/main: autovacuum launcher
             ├─531757 postgres: 13/main: stats collector
             └─531758 postgres: 13/main: logical replication launcher

Mar 27 08:14:01 debian systemd[1]: Starting PostgreSQL Cluster 13-main...
Mar 27 08:14:03 debian systemd[1]: Started PostgreSQL Cluster 13-main.
```

--------------------------------------------------------------------------------



## Практика+

Задания:
1. Установите параметр work_mem = 32MB в командной строке
   запуска утилиты psql.
2. В пакетном дистрибутиве для Ubuntu файл postgresql.conf
   находится не в каталоге PGDATA. Каким образом сервер
   находит этот файл конфигурации при запуске?

#### Коментарии:

1. Используйте один из двух способов: ключ options в строке подключения или
   переменная среды PGOPTIONS.
   Информация о формировании строки подключения:
   https://postgrespro.ru/docs/postgresql/13/libpq-connect#LIBPQ-CONNSTRING

2. Расположение файла postgresql.conf можно посмотреть в параметре config_file.
   Чтобы узнать, где задается этот параметр, посмотрите вывод команды ps для
   основного процесса postgres. Идентификатор процесса (PID) записан в первой
   строке файла postmaster.pid, который находится в каталоге с данными (PGDATA).


--------------------------------------------------------------------------------


## Моё решение



#### 1я задача через EnvVar PGOPTIONS:
```sh
PGOPTIONS="-c work_mem=32MB" psql
Timing is on.
psql (13.14 (Debian 13.14-0+deb11u1))
Type "help" for help.

postgres@[local] ~=# SHOW work_mem;
 work_mem
----------
 32MB
(1 row)

Time: 0.241 ms
```


#### 2я Задача - откуда сервер берёт основной конфиг если его нет в PGDATA:

```sh
# узаню путь к PGDATA
postgres@debian:~$ psql -c 'SHOW data_directory'
Timing is on.
       data_directory
-----------------------------
 /var/lib/postgresql/13/main
(1 row)

# узнаю pid сервера(1я строка в файле postmaster.pid который всегда в PGDATA)
postgres@debian:~$ cat /var/lib/postgresql/13/main/postmaster.pid
531751
/var/lib/postgresql/13/main
1711516441
5432
/var/run/postgresql
localhost
   265304    393256
ready

## Смотрю как был запущен этот процесс
postgres@debian:~$ ps -p 531751 -o comm,args
COMMAND   COMMAND
postgres  /usr/lib/postgresql/13/bin/postgres -D /var/lib/postgresql/13/main \
                         -c config_file=/etc/postgresql/13/main/postgresql.conf
```
Вот и ответ через ключ `-c` настраивается значение config_file откуда затем
сервер и подтягивает все настройки. не как обычно из PGDATA а из /etc/postgresql

второй более простой способ узанть это же - через
```sh
sudo systemctl status postgresql@13-main
● postgresql@13-main.service - PostgreSQL Cluster 13-main
     Loaded: loaded ...
     Active: active ...
    Process: 531746 ...
   Main PID: 531751 (postgres)
         ...
     CGroup: /system.slice/system-postgresql.slice/postgresql@13-main.service
          ├─531751 /usr/lib/postgresql/13/bin/postgres \
                         -D /var/lib/postgresql/13/main \
                         -c config_file=/etc/postgresql/13/main/postgresql.conf
```
тоже видно что запуск идёт через `-c config_file=кастомный-путь-к-конфигу`



## Ответы авторов на Практика+

### Задача 1. Установка параметров при запуске приложения

Если приложение использует библиотеку `libpq` для подключения к серверу,
установить параметры при запуске можно двумя способами.
Первый способ — указать ключ options в строке параметров подключения:

```sh
student$ psql "options='-c work_mem=32MB'" -c 'SHOW work_mem'
work_mem
----------
32MB
(1 row)
```

Второй способ — установить переменную окружения PGOPTIONS:
```sh
student$ export PGOPTIONS='-c work_mem=32MB'; psql -c 'SHOW work_mem'
work_mem
----------
32MB
(1 row)
```


### Задача 2. Где определяется config_file

Файл postgresql.conf расположен не в каталоге с данными:
```sh
=> SHOW config_file;
config_file
-----------------------------------------
/etc/postgresql/13/main/postgresql.conf
(1 row)
=> SHOW data_directory;
data_directory
-----------------------------
/var/lib/postgresql/13/main
(1 row)
```
Каким же образом сервер находит postgresql.conf?
В пакетном дистрибутиве для Ubuntu значение параметра config_file указано в
командной строке запуска сервера.
Это позволяет перенести postgresql.conf в каталог, отличный от PGDATA.

Команду для запуска сервера можно найти в исходном коде утилиты pg_ctlcluster,
а можно посмотреть в описании процесса postgres.
Идентификатор (PID) основного процесса сервера (обычно называемого postmaster),
записан в первой строке файла `postmaster.pid`.
Этот файл всегда расположен в каталоге `PGDATA`.
```sh
student$ sudo cat /var/lib/postgresql/13/main/postmaster.pid
28650
/var/lib/postgresql/13/main
1705390505
5432
/var/run/postgresql
localhost
655372
32775
ready

student$ ps 28650
PID TTY
28650 ?
STAT
Ss
TIME COMMAND
0:00 /usr/lib/postgresql/13/bin/postgres -D /var/lib/postgresql/13/main \
                         -c config_file=/etc/postgresql/13/main/postgresql.conf
```



