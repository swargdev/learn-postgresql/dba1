## DBA1-13. Тема 00. Введение
https://postgrespro.ru/education/courses/DBA1
видео: yevXLP2LA4Q 2022-07-12T04:09

## Структура курса

> День 1
- Базовый инструментарий
    01. Установка и управление сервером
    02. Использование psql
    03. Конфигурирование
- Архитектура
    04. Общее устройство PostgreSQL
    05. Изоляция и многоверсионность
    06. Очистка

> День 2
- Архитектура (продолжение)
    07. Буферный кеш и журнал
- Организация данных
    08. Базы данных и схемы
    09. Системный каталог
    10. Табличные пространства
    11. Низкий уровень

- Задачи администрирования
    12. Мониторинг
- Управление доступом
    13. Роли и атрибуты

> День 3
- Управление доступом (продолжение)
    14. Привилегии
    15. Политики защиты строк
    16. Подключение и аутентификация
- Резервное копирование
    17. Обзор
- Репликация
    18. Обзор


