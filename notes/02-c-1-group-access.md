## Здесь мой опыт настройки доступа к psql и настройки групп и прав доступа


- PostgreSQL по умолчанию работает от лица юзера postgres
- войти в бд через psql от лица текущего пользователя в системе не выйдет
- PostgreSQL имеет настройки доступа к бд и они хранятся в конфиг файле
  `/etc/postgresql/13/main/pg_hba.conf` и по умолчанию разрешают доступ к
  бд только либо от лица системного юзера postgres либо при явном указании хоста
  127.0.0.1 + потребует ввода пароля пользователя postgres.
- на начальных этапах проще если можно дать своему юзеру доступ в группу sudo
  а затем входить в систему от лица юзера postgres через `sudo -su postgres`
  и уже в новой сесси взаимодействовать с `psql`

Что здесь описано:
- сдесь собран мой опыт подключения к psql
- как проверить в каких группах состоит конкретный пользователь ОС
  (a-check-user-groups)
-

попытка подключения
```sh
user@debian:~$ psql
psql: error: FATAL:  role "user" does not exist
```

Это говорит о том, что у пользователя с именем `user` нет прав для подключения
к PostgreSQL. Как помним сам PostgreSQL работает от лица пользователя `postgres`
поэтому для доступа к бд нужно взаимодействовать с ним тоже от лица этого же
пользователя.

Причем по умолчанию при установке из пакета метод авторизации установлен в peer
Это значит что пускать будет только если программу psql будет запускать
конкретный прописанный в конфиге юзер `postgres`.


Попытка подключится к серверу PostgreSQL через psql работая от имени своего
основного пользователя не от postgres.

1. попытка просто указать пользователя от лица которо хотим войти в БД
```sh
user@debian:~$ psql -U postgres
psql: error: FATAL:  Peer authentication failed for user "postgres"
```
это говорит о том, что у текущего пользователя с именем `user` нет прав доступа
к ресурсам пользователя с именем `postgres`(?)

можно войти в систему от лица пользователя `postgres` через sudo
но для этого текущий пользователь должен быть прописан в группе sudo

Это можно проверить так:                                    a-check-user-groups
```sh
grep sudo /etc/group
sudo:x:27:user
# ^       ^^^^ имена пользователей входящие в эту группу
#  `название группы
```

если текущего пользователя нет в группе sudo можно добавить его так:
Еще одна команда для добавления пользователя в группу sudo
```sh
sudo adduser <username> sudo
```

еще одна команды для добавления пользователя с именем 'username' в группу sudo
```sh
sudo usermod -aG sudo <username>
```
ВАЖНО:
флаг `a` очень важен - без него пользователь будет удалён из всех остальных групп.


Дальше чтобы настройка групп вступила в силу нужно будет либо перезапустить
оболочку/терминал, либо выйти из системы и снова войти(login). (см man usermod)
(Под графической системой в Linux можно просто открыть новое окно терминала.)

Есть так же команда `newgrp` позволяющая обносить группы безе перезахода
Try the `newgrp` command to log in to a new group without
logging in again or reboot the system



По логике можно было бы попытся вместо того чтобы давать своему юзер рут-доступ
добавить его в группу postgres и затем входить в psql вот так
```sh
psql -U postgres
```
но по умолчанию это не будет работать т.к. нужны доп. настройки прав доступа к
бд. Более подробно ниже



## Дополнительно про группы

- /etc/group - системный файл хранящий какие группы в системе есть и кто в них
входит

узнать все группы в которые входит указанный пользоаватель с именем `user`

```sh
grep user /etc/group
sudo:x:27:user
audio:x:29:pulse,user
video:x:44:user
lpadmin:x:121:user
user:x:1000:
...

# какие пользователи входят в группу postgres
grep postgres /etc/group
ssl-cert:x:115:postgres
postgres:x:133:
#               ^ ни одного нет
# ^ имя группы одноименное для имени пользователя `postgres`

# добавляем текущего пользователя в группу postgres
sudo adduser user postgres
Adding user `user' to group `postgres' ...
Adding user user to group postgres
Done.

# перепроверяем еще раз кто может ходить от лица группы postgres
grep postgres /etc/group
ssl-cert:x:115:postgres
postgres:x:133:user
#              ^^^^

# по идеи теперь должно пускать в БД вот так, но нет - ошибка авторизации
psql -U postgres
psql: error: FATAL:  Peer authentication failed for user "postgres"

# а вот так спрашивает пароль от пользователя postgres
psql -U postgres -h 127.0.0.1
Password for user postgres:
psql: error: fe_sendauth: no password supplied
```
Почему запуск psql ведёт себя менно так:
- когда запускаешь psql -U postgres то это авторизация типа peer, при этом
  проверка авторизации юзера возлагается на ОС. т.е. нужно чтобы текущий
  юзер был именно `postgres` т.е. тот который указываем в флаге `-U`
  здесь же пытаемся войти работая в системе от лица `user` поэтому не пускает
- когда явно указываем еще и хост 127.0.0.1(Локальный хост) то это автоматом
  с конфига подхватывает правило - справшивать пароль(md5)


Всё это поведение проверки авторизации настраивается через конфиг-файл
/etc/postgresql/13/main/pg_hba.conf

Кратко о том как можно посмотреть что там настроено.

Выводим последнии 22 строчки из конфиг файла
```sh
sudo tail -n 22 /etc/postgresql/13/main/pg_hba.conf

# DO NOT DISABLE!
# If you change this first entry you will need to make sure that the
# database superuser can access the database using some other method.
# Noninteractive access to all databases is required during automatic
# maintenance (custom daily cronjobs, replication, and similar tasks).
#
# Database administrative login by Unix domain socket
local   all             postgres                                peer

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     peer
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
host    replication     all             ::1/128                 md5
```

Документация о методах авторизации к сереру
https://www.postgresql.org/docs/13/auth-methods.html

если кратко то в поля TYPE опеределяют то как идёт подключение через psql
local - когда вызываем без указания конкретного хоста(т.е. без флага -h)

Поэтому вот эта строка
```sh
# Database administrative login by Unix domain socket
local   all             postgres                                peer
# TYPE  DATABASE        USER            ADDRESS                 METHOD
```
означает
- TYPE     : local     при подключении с локальной машины
- DATABASE : all       доступ в любые базы
- USER     : postgres  только при запуске проги от лица ОС юзера `postgres`
- ADDRESS  :           пусто т.к. речь о локальном подключении
- METHOD   : peer      вот это и есть способ авторизации. Смотрим доки

Доки про все типы авторизации:
https://www.postgresql.org/docs/13/auth-methods.html

конктерно про метод peer
https://www.postgresql.org/docs/13/auth-peer.html

Кратко оригинал:
- Peer authentication, which relies on operating system facilities to identify
the process at the other end of a local connection.
This is not supported for remote connections.
Перевод:
- Одноранговая аутентификация, которая использует возможности операционной
системы для идентификации процесса на другом конце локального соединения.
Это не поддерживается для удаленных подключений.

pqsl - это клиентская программа. как любая программа она запускается внутри
процесса операционной системы. Каждый процесс запускается от лица конкретного
пользователя. В обычных условиях когда идёт работа в командной строке то
процессу передаётся тот пользователь который этот процесс и запустил.
Поэтому суть метода peer - это просто проверять кто именно запустил pqsl и если
это не postgres (как настроено в этом конфиге) то запрещать такое подключение.

О том почему спрашивает пароль когдя указываем `-U 127.0.0.1`
это прописано через строку
```sh
# TYPE  DATABASE        USER            ADDRESS                 METHOD
host    replication     all             127.0.0.1/32            md5
```

METHOD:md5 - Это метод авторизации при котором спрашивается пароль.
вот и получается - указал хост - сработало правило "подключение по адресу, а
не локальное подключение с такого-то адреса"

https://www.postgresql.org/docs/13/auth-methods.html:
- Password authentication, which requires that users send a password.

https://www.postgresql.org/docs/13/auth-password.html

Выдержка из доки:
> md5
The method md5 uses a custom less secure challenge-response mechanism.
It prevents password sniffing and avoids storing passwords on the server
in plain text but provides no protection
if an attacker manages to steal the password hash from the server.
Also, the MD5 hash algorithm is nowadays no longer considered secure
against determined attacks.

The md5 method cannot be used with the `db_user_namespace` feature.
https://www.postgresql.org/docs/13/runtime-config-connection.html#GUC-DB-USER-NAMESPACE

To ease transition from the `md5` method to the newer `SCRAM` method,
if md5 is specified as a method in pg_hba.conf but the user's password
on the server is encrypted for SCRAM (see below), then
SCRAM-based authentication will automatically be chosen instead.



