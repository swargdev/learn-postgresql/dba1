
## Кратко содержимое урока:
- два варианта установки PostgreSQL пакеты или исходный код
- PostgreSQL "в облаках"
- сервер PostgreSQL всегда работает от лица выделенного пользователя
  спрокетирован так чтобы не работать от root(в целях безопастности)
- сразу после установки ПО PostgreSQL нужно инициализировать кластер баз данных
- Команды управления сервером pg_ctl pg_ctlcluster


--------------------------------------------------------------------------------
- Основные понятия
- Установка из исходников
- Управление сервером
- Пакетная установка
- Управление сервером в Ubuntu
- Облачные решения

--------------------------------------------------------------------------------

- PostgreSQL - это СУБД сервер-программа управляющая нескольким БД.
- кластер базданных - это несколько БД внутри одного СУБД PostgreSQL
  в понятийной базе PostgreSQL - классер базданных - немного сбивает с толку
  т.к. в постгрессе кластер - это всего лишь список БД управляемых одной СУБД
  баз данных которые обслуживает один PostgreSQL-сервер(программа)

- initdb - нельзя запустить из-под рута. т.к. PostgreSQL обычно ставят так
         чтобы он работал от лица отдельного пользователя БД
- PGDATA - (EnvVar) каталог где будут храниться бд постгресса кластер БазДанных

initdb по умолчанию инициализирует кластер базданных без подсчёта контрольных
сум. Эти контрольные суммы используются для проверки цельности записываем данных.
т.е. когда констроль сумм выключён, то постгрес не будет проверять целостность
данных. Эта контрольные суммы позволяют узнать о проблемах с Файловой Системой
на ранних этапах, поэтому лучше его включать сразу.

ручное создание каталога `PGDATA`. сюда будем инициализировать кластер базданных
```sh
whoim # -> student
initdb -U postgres -k -D /home/student/pgsql13/data
export PGDATA=/home/student/pgsql13/data
export PATH=/home/student/pgsql13/bin:$PATH

# initialize dbs-cluster
initdb -U postgres -k -D /home/student/pgsql13/data
# -U - инициализировать от имени конкретного пользователя OS, здесь это postgres
# -k - включение контрольных сумм
# -D если не указать явно будет взято из EnvVar PGDATA
```

## Особенности Установки и Управление PostgreSQL в Ubuntu

вместо initdb используем pg_createcluster
это обёртка над оригинальным initdb, имеющая некие особенности:

- при установке сервера PostgreSQL через пакетные менеджеры идёт автоматическая
  инициализация кластера базданных под именем "main".
- но подсчёт контрольных сумм не включается!
  где конфиги? где журнал сервера?
- автозапуск при установке из пакета для дистрибутива(Ubuntu) прописывается сам

вот при установке мне отписало такое:
Success. You can now start the database server using:

    pg_ctlcluster 13 main start

## Консоль. Применяем полученную теорию на практике

## Создаём кластер базданных под Ubuntu

где лежат файлы (установленного в систему PostgreSQL)
```sh
sudo ls -l /usr/lib/postgresql/13/
total 8
drwxr-xr-x 2 root root 4096 Mar 20 12:16 bin
drwxr-xr-x 4 root root 4096 Mar 20 12:16 lib
```

Узнать конкретные параметры с которыми собирался установленный постгрес-сервер
```sh
sudo /usr/lib/postgresql/13/bin/pg_config --configure
```

Смотрим на содержимое каталога куда был проинициализирован кластер-бдх (PGDATA):
```sh
sudo ls -ls /var/lib/postgresql/13/main/
4 drwx------ 5 postgres postgres 4096 Mar 20 12:16 base
4 drwx------ 2 postgres postgres 4096 Mar 20 12:32 global
4 drwx------ 2 postgres postgres 4096 Mar 20 12:16 pg_commit_ts
4 drwx------ 2 postgres postgres 4096 Mar 20 12:16 pg_dynshmem
4 drwx------ 4 postgres postgres 4096 Mar 20 12:21 pg_logical
4 drwx------ 4 postgres postgres 4096 Mar 20 12:16 pg_multixact
4 drwx------ 2 postgres postgres 4096 Mar 20 12:16 pg_notify
4 drwx------ 2 postgres postgres 4096 Mar 20 12:16 pg_replslot
4 drwx------ 2 postgres postgres 4096 Mar 20 12:16 pg_serial
4 drwx------ 2 postgres postgres 4096 Mar 20 12:16 pg_snapshots
4 drwx------ 2 postgres postgres 4096 Mar 20 12:16 pg_stat
4 drwx------ 2 postgres postgres 4096 Mar 20 12:16 pg_stat_tmp
4 drwx------ 2 postgres postgres 4096 Mar 20 12:16 pg_subtrans
4 drwx------ 2 postgres postgres 4096 Mar 20 12:16 pg_tblspc
4 drwx------ 2 postgres postgres 4096 Mar 20 12:16 pg_twophase
4 -rw------- 1 postgres postgres    3 Mar 20 12:16 PG_VERSION
4 drwx------ 3 postgres postgres 4096 Mar 20 12:16 pg_wal
4 drwx------ 2 postgres postgres 4096 Mar 20 12:16 pg_xact
4 -rw------- 1 postgres postgres   88 Mar 20 12:16 postgresql.auto.conf
4 -rw------- 1 postgres postgres  130 Mar 20 12:16 postmaster.opts
4 -rw------- 1 postgres postgres  110 Mar 20 12:16 postmaster.pid

sudo du -hcs /var/lib/postgresql/13/main/
40M	total
```
Обрати внимание файлы в каталоге с кластером принадлежат не `root` а
пользователю `postgres`. Т.е. сами исполняемые файлы PostgreSQL устанавливаются
с правами root а данные кластера базданных с правами специально выделенного под
это дело системного пользователя `postgres`

## Управление Сервером как запускать и останавливать
- Запуск сервера уже прописан в автозапуске (при установке из пакета дистр-ва)

`pg_ctl` - утилита идущая в самом PostgreSQL
решает задачи:
- перезапустить, остановить, запустить, переключиться на реплику
- сама автоматически подставляет нужного пользователя(postgres а не root)
  для запуска сервера т.е. `sudo -u postgres .. cmd`

в пакетах для дистрибутивов могут быть свои обёртки над утилитой `pg_ctl`:
- в Debian,Ubuntu это `pg_ctlcluster`

sudo -u postgres psql

```sh
which pg_ctlcluster
/usr/bin/pg_ctlcluster
```

чтобы иметь доступ к pg_ctl нужно прописать в PATH каталог куда установило+bin:
```sh
PATH="$PATH:/usr/lib/postgresql/13/bin/"
```

### Остановка и Запуск кластера
- sudo pg_createcluser 13 main stop
- sudo pg_ctlcluster 13 main start
- sudo pg_ctlcluster 13 main restart  - перезапустить сервер.
- sudo pg_ctlcluster 13 main reload   - перечитать конфигурацию.

`pg_ctlcluster` умеет управлять сразу несколькими кластерами баз данных, причем
разных версий PostgreSQL. Поэтому и нужно указывать сначала версию PostgreSQL
(здесь это `13`) и, затем, имя самого кластера баз данных (здесь это `main`)

Важный момент. Утилита `pg_ctlcluster` удобна тем, что при запуске от лицв
root(sudo) сама выставляет правильные права доступа то есть запускает сервер от
postgres а не root.

```sh
sudo pg_ctlcluster 13 main status
pg_ctl: server is running (PID: 152907)
/usr/lib/postgresql/13/bin/postgres "-D" "/var/lib/postgresql/13/main" "-c" "config_file=/etc/postgresql/13/main/postgresql.conf"
```


## Логи - Журнал сообщений сервера. Расположение при установке из пакета.

```sh
ls -l /var/log/postgresql/postgresql-13-main.log
-rw-r----- 1 postgres adm 1704 Mar 20 12:50 /var/log/postgresql/postgresql-13-main.log
```
```sh
2024-03-20 12:50:27.537 MSK [152907] LOG:  starting PostgreSQL 13.14 (Debian 13.14-0+deb11u1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 10.2.1-6) 10.2.1 20210110, 64-bit
2024-03-20 12:50:27.538 MSK [152907] LOG:  listening on IPv6 address "::1", port 5432
2024-03-20 12:50:27.538 MSK [152907] LOG:  listening on IPv4 address "127.0.0.1", port 5432
2024-03-20 12:50:27.573 MSK [152907] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
2024-03-20 12:50:27.664 MSK [152908] LOG:  database system was shut down at 2024-03-20 12:47:12 MSK
2024-03-20 12:50:27.703 MSK [152907] LOG:  database system is ready to accept connections
```



## Практика

## Задание 1 Включение расчёта контрольных сумм в кластере.

1. Останови сервер
2. Проверь идёт ли расчёт контрольных сумм(контролек) в кластере
3. Включи расчёт контрольных сумм.
4. Запусти сервер
Доп материалы - важные комментарии есть есть внизу под слайдом в материалах курса.
Там есть то, как выполнить какую-нибудь не очевидную операцию

## Комментарии авторов:
2, 3. Используйте утилиту pg_checksums.
Для ее запуска укажите полный путь: /usr/lib/postgresql/13/bin/pg_checksums
https://postgrespro.ru/docs/postgresql/13/app-pgchecksums


## Практика+

Установка PostgreSQL из исходников. Инициализация кластера базданных.

[мои решения](./practice/01-p-pg_ctlcluster.md)
