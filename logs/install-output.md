Creating config file /etc/postgresql-common/createcluster.conf with new version
Building PostgreSQL dictionaries from installed myspell/hunspell packages...
ERROR: no ecoding defined in /usr/share/hunspell/ar.aff, ignoring
  be_by
  bg_bg
  bs_ba
  ca
  ca_es-valencia
  cs_cz
  da_dk
  de_at
  de_ch
  de_de
  el_gr
  en_gb
  en_us
  eo
  es
  et_ee
  eu
  fa_ir
  fr
  ga_ie
  gl_es
  gu_in
  he
  hi_in
  hr_hr
  hu_hu
iconv: illegal input sequence at position 131
ERROR: Conversion of /usr/share/hunspell/hu_HU.aff failed
  id_id
  is_is
  it_it
ERROR: no ecoding defined in /usr/share/hunspell/kk_KZ.aff, ignoring
  kmr_latn
  ko
  lt_lt
  lv_lv
  ml_in
  nb_no
  ne_np
  nl
  nn_no
  pl_pl
  pt_br
  pt_pt
  ro_ro
  ru_ru
  si_lk
  sk_sk
  sl_si
  sq_al
  sr_latn_rs
  sr_rs
  sv_fi
  sv_se
  te_in
  th_th
  uk_ua
  vi_vn
Removing obsolete dictionary files:
Created symlink /etc/systemd/system/multi-user.target.wants/postgresql.service → /lib/systemd/system/postgresql.service.
Setting up postgresql-13 (13.14-0+deb11u1) ...
Creating new PostgreSQL cluster 13/main ...
/usr/lib/postgresql/13/bin/initdb -D /var/lib/postgresql/13/main --auth-local peer --auth-host md5
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locale "en_US.UTF-8".
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

Data page checksums are disabled.

fixing permissions on existing directory /var/lib/postgresql/13/main ... ok
creating subdirectories ... ok
selecting dynamic shared memory implementation ... posix
selecting default max_connections ... 100
selecting default shared_buffers ... 128MB
selecting default time zone ... Europe/Moscow
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... ok
syncing data to disk ... ok

Success. You can now start the database server using:

    pg_ctlcluster 13 main start

Ver Cluster Port Status Owner    Data directory              Log file
13  main    5432 down   postgres /var/lib/postgresql/13/main /var/log/postgresql/postgresql-13-main.log
update-alternatives: using /usr/share/postgresql/13/man/man1/postmaster.1.gz to provide /usr/share/man/man1/postmaster.1.gz (p
ostmaster.1.gz) in auto mode
